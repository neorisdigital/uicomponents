// Use my coding convention in my components pls goddamit
import { Component, EventEmitter, Output }  from '@angular/core'
import { TranslationService }             from '../../services/translation.service';
import { Logger }                         from '../../services/logger.service';
import { Broadcaster }                    from '../../../uicomponents/events/broadcaster.event';
import JobsiteDTO from '../../DTO/jobsiteDTO';

// Calendar modifies stuff from core services
import { CalendarEvents } from '../../events/calendar.event';

var moment = require('moment');

@Component({
    selector: 'calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
    providers: [TranslationService]
})

export class CalendarComponent {
    public defaultDate: any = moment().format("DD/MM/YYYY");

    constructor(private t: TranslationService,
                private eventDispatcher: Broadcaster,
                private calendar: CalendarEvents) {}
    
    dateSelected(event) {
        this.calendar.setDates(event);
    }
    
    // Calendar
    pickerOptions: Object = {
        'showDropdowns': false,
        'showWeekNumbers': false,
        'autoApply': false,
        linkedCalendars: false,
        // 'startDate': '05/28/2016',
        // 'endDate': '06/03/2016'
    };
}