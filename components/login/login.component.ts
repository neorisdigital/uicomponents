import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { HttpCemex } from '../../services/http.service';
import { CredentialsConstants } from '../../constants/credentials.constants';
import { Broadcaster } from '../../events/broadcaster.event';
import { Logger } from '../../services/logger.service';
import { TranslationService } from '../../services/translation.service';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [HttpCemex, Broadcaster, TranslationService],
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username = "";
    password = "";
    loginError = false;
    message: string;

    language:string = "EN";

    enflagged:boolean = true;
    esflagged:boolean = false;

    ngOnInit(): void {
        this.authService.logout();

        this.eventDispatcher.on<string>(SessionService.LOGIN_FAIL_EVENT)
            .subscribe(response => {
                this.loginError = true;
                this.setMessage();
            });
        this.eventDispatcher.on<string>(SessionService.LOGIN_SUCCESS_EVENT)
            .subscribe(response => {
                this.loginError = false;
                this.setMessage();
                this.router.navigate(['home']);
            });
    }

    constructor(private authService: SessionService,
        private eventDispatcher: Broadcaster,
        private t: TranslationService,
        private router: Router) {
        var test= undefined;

    }

    login() {
        this.authService.login(this.username, this.password);
    }

    logout(): void {
        this.authService.logout();
    }

    setMessage() {
        this.message = 'Logged ' + (SessionService ? 'in' : 'out');
        Logger.log(this.message);
    }

    changeLanguage(lang:any) {
      this.t.lang(lang);
      if (lang == 'es') {
        this.language = 'ES';
        this.esflagged = true;
        this.enflagged = false;
      } else {
        this.language = 'EN';
        this.enflagged = true;
        this.esflagged = false;
      }
    }
}
