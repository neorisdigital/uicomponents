import { Component,EventEmitter, Output, OnInit } from '@angular/core'
import { Broadcaster} from '../../events/broadcaster.event';
import {TranslationService} from '../../services/translation.service';
import {Logger} from '../../services/logger.service';
let _  = require('underscore');
@Component({
  selector:     'filter',
  templateUrl:  './filter.component.html',
  providers: [Broadcaster, TranslationService],
  styleUrls:   ['./filter.component.scss']
})

export class FilterComponent implements OnInit {
  status:any
  types:any
  typeText:string;
  statusText:string;
  viewActive:string;
  allStatus:any;
  allTypes:any;
  statusList:any = [];
  typeList:any = [];

  static CHANGE_TYPE_FILTER     = "CHANGE_TYPE_FILTER";
  static CHANGE_STATUS_FILTER   = "CHANGE_STATUS_FILTER";
  static CHANGE_CARDS_STYLE     = "CHANGE_CARDS_STYLE";

  constructor(private eventDispatcher: Broadcaster,  private t:TranslationService){
    this.typeText = "";
    this.statusText = "";
    this.viewActive = 'cards';
    this.status = [
                    { text: 'Active',           value:'NST' },
                    { text: 'In Progress',      value:'STR' },
                    { text: 'To be Confirmed',  value:'WCL' },
                    { text: 'On Hold',          value:'OHD' },
                    { text: 'Cancelled',        value:'CNC' },
                    { text: 'Completed',        value:'CMP' },
                    { text: 'Payment Hold',     value:'PHD' }
    ];
    this.types = [
            { text: 'Delivery', value:'NST' },
            { text: 'Pickup',   value: 'STR'}
    ];

  }

  ngOnInit(){
    this.setDefaults();
  };

  private setDefaults():void {
    //logic for set default item
    for(let status of this.status){
      status.selected = true;
      this.statusList.push(status.value);
    }

    for(let type of this.types){
      type.selected = true;
      this.typeList.push(type.text);
    }
    this.allStatus = true;
    this.allTypes = true;
    this.statusText = "All status";
    this.typeText = "All types";
    this.eventDispatcher.broadcast(FilterComponent.CHANGE_TYPE_FILTER, this.typeList);
    this.eventDispatcher.broadcast(FilterComponent.CHANGE_STATUS_FILTER, this.statusList);
  }

  public changeAll(flag, id):void{
      if(id == "status"){
        this.statusList = [];        
        this.statusText = "All status";
         for(let status of this.status){
          status.selected = flag;
          this.updateStatusList(status);
          this.eventDispatcher.broadcast(FilterComponent.CHANGE_STATUS_FILTER, this.statusList);
        }
      };

      if(id == "type"){
        this.typeList = [];        
        this.typeText = "All types";
         for(let type of this.types){
          type.selected = flag;
         this.updateTypeList(type);
         this.eventDispatcher.broadcast(FilterComponent.CHANGE_TYPE_FILTER, this.typeList);         
        }
      };
  }

  public selectAll(id):void{
    if(id == "status"){
      this.allStatus = !this.allStatus;
       this.changeAll(this.allStatus, id);
    };

    if(id == "type"){
       this.allTypes = !this.allTypes;
        this.changeAll(this.allTypes, id);
    }
  }

  public selectItem(id, item):void {
      item.selected = !item.selected
      if(id === 'type'){
        this.allTypes = false;
        this.updateTypeList(item);
        this.changeTexts(id);
        this.eventDispatcher.broadcast(FilterComponent.CHANGE_TYPE_FILTER, this.typeList);
      }
      if(id === 'status'){
        this.allStatus = false;
        this.updateStatusList(item);
        this.changeTexts(id);
        this.eventDispatcher.broadcast(FilterComponent.CHANGE_STATUS_FILTER, this.statusList);        
      }
  }

  public changeTexts(id:any):void{
    if(id == "status"){
        if(this.statusList.length == 1){
          let item =  _.find(this.status, function(status){ return status.value == this.statusList; }, this);
          this.statusText = item.text;
        }
        else if(this.statusList.length > 1){
           this.statusText = this.statusList.length + " Types"
        }
        else{
           this.statusText = "All status";
        }
    }
    if(id == "type"){
       if(this.typeList.length == 1){
          let item =  _.find(this.types, function(type){ return type.text == this.typeList; }, this);
          this.typeText = item.text;
        }
        else if(this.typeList.length > 1){
           this.typeText = this.typeList.length + " Types"
        }
        else{
           this.typeText = "All types";
        }
    }
    
  }

  public changeTo(id):boolean{
    this.viewActive = id;
    this.eventDispatcher.broadcast(FilterComponent.CHANGE_CARDS_STYLE, id);
    return false;
  }

  public updateStatusList(pod):void{
     if(pod.selected){
      this.statusList.push(pod.value);
    }
    else{
      let index = this.statusList.indexOf(pod.value);
      if(index != -1){
        this.statusList.splice(index, 1)
      }
    }
  }

  public updateTypeList(type):void{
     if(type.selected){
      this.typeList.push(type.text);
    }
    else{
      let index = this.typeList.indexOf(type.text);
      if(index != -1){
        this.typeList.splice(index, 1)
      }
    }
  }

}
