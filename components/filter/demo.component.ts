import { Component } from '@angular/core'
import { Broadcaster } from '../../events/broadcaster.event';
import { FilterComponent } from './filter.component'
import {TranslationService} from '../../services/translation.service';

@Component({
  selector: 'demo-filter',
  templateUrl: './demo.component.html',
  styleUrls:   ['./demo.component.scss'],
  providers: [FilterComponent, TranslationService]
})
export class DemoFilterComponent {

  constructor(private ev: Broadcaster) {
    this.ev.on<any>(FilterComponent.CHANGE_TYPE_FILTER)
      .subscribe(response => this.onTypeChange(response));

    this.ev.on<any>(FilterComponent.CHANGE_STATUS_FILTER)
      .subscribe(response => this.onStatusChange(response));

    this.ev.on<any>(FilterComponent.CHANGE_CARDS_STYLE)
      .subscribe(response => this.onCardStyleChange(response));
  }

  public onStatusChange = function(response){
    console.log("new status: " + response.text);
    return false;
  }

  public onTypeChange = function(response){
    console.log("new type: " + response.text);
    return false;
  }

  public onCardStyleChange = function(response){
    console.log(response);
    return false;
  }
}