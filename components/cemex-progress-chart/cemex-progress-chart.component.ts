import { Component,Input } from '@angular/core';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';
// webpack html imports
let template = require('./cemex-progress-chart.component.html');

@Component({
  selector: 'cemex-progress-chart',
  template: template,
  styleUrls: ['./cemex-progress-chart.component.scss']
})
export class CemexProgressChartComponent {
  @Input() chartData:any[];
  @Input() totalOrders:number;

  private backgroundColors:string[]=["progress-bar-success","progress-bar-info","progress-bar-warning","progress-bar-danger"];

  constructor(private t:TranslationService){
    if(this.chartData==undefined || this.chartData==null){
      this.chartData=[];
    }
    if(this.chartData.length==0){
      this.chartData=[{title:"Data 1",progress:20,color:"#b8436d"},{title:"Data 2",progress:30,color:"#00d9f9"},{title:"Data 3",progress:40,color:"#a4c73c"}];
    }
  }

  private getClass(index:number):string{
    if(index>3){
      this.backgroundColors[index%4];
    }
    return this.backgroundColors[index];
  }
  
}