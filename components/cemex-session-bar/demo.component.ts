import { Component } from '@angular/core';

@Component({
    selector: 'cemex-session-bar',
    templateUrl: './cemex-session-bar.component.html',
    styleUrls: [ './cemex-session-bar.component.scss' ]
})
export class DemoCemexSessionBarComponent{
    private _imagePath = require( '../../../../wwwroot/vendor/images/logo-cemex-white.png' );
}