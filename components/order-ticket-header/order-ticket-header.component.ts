import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TranslationService }             from '../../services/translation.service';
import { Logger }                         from '../../services/logger.service';
import { Broadcaster }                    from '../../../uicomponents/events/broadcaster.event';
import { orderCardComponent } from '../../../uicomponents/components/ordercard/ordercard.component';

import JobsiteDTO from '../../DTO/jobsiteDTO';

@Component({
    selector: 'order-ticket-header',
    templateUrl: './order-ticket-header.component.html',
    styleUrls: ['./order-ticket-header.component.scss'],
    providers: [TranslationService]
})

export class OrderTicketHeaderComponent {
	static SELECTED_FILTER = "SELECTED_FILTER_ORDERDT";

    public filterTab:any;

    isRMSDelivery: boolean;

    constructor(private t: TranslationService, private ev: Broadcaster) {
        this.ev.on<any>(OrderTicketHeaderComponent.SELECTED_FILTER).subscribe(response => {
            this.filterTab = response;
            console.log(response);
        });
        console.log('hey gurl', orderCardComponent.lastSelectedOrder)
        if(orderCardComponent.lastSelectedOrder.isReadyMix == true && orderCardComponent.lastSelectedOrder.shipping == "Delivery"){
            this.isRMSDelivery = true;
            this.filterTab='maptracking';
        } else {
            this.isRMSDelivery = false;
            this.filterTab='listtrucks';
        }
    }

    public toggleFilter(tab:any){
        this.ev.broadcast(OrderTicketHeaderComponent.SELECTED_FILTER, tab)
    }
    public toggleFilter2(tab:any){
        this.ev.broadcast(OrderTicketHeaderComponent.SELECTED_FILTER, tab)
    }
}