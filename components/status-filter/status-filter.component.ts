import { Component,EventEmitter,Output } from '@angular/core'
import { Broadcaster} from '../../events/broadcaster.event';
import {TranslationService} from '../../services/translation.service';
import {Logger} from '../../services/logger.service';

@Component({
  selector:     'status-filter',
  templateUrl:  './status-filter.component.html',
  providers: [Broadcaster, TranslationService],
  styleUrls:   ['./status-filter.component.scss']
})

export class StatusFilterComponent {
  allSelected:boolean = false;
  selected:boolean = false;

  allStatus: boolean = true;
  oneStatus: boolean = false;

  status:any;
  filter:any;

  whoIsChecked = []

  static SELECTED_STATUS_FILTER = "SELECTED_STATUS_FILTER";

  constructor(private eventDispatcher: Broadcaster, private t: TranslationService){
    this.status = [
                    { title: 'Active',           color:'NST' },
                    { title: 'In Progress',      color:'STR' },
                    { title: 'To be Confirmed',  color:'WCL' },
                    { title: 'On Hold',          color:'OHD' },
                    { title: 'Cancelled',        color:'CNC' },
                    { title: 'Completed',        color:'CMP' },
                    { title: 'Payment Hold',     color:'PHD' }
                  ]

    for (let i = 0; i < this.status.length; i++) {
          this.whoIsChecked.push(false);
        }
  }


  //for Status
  public checkAll():void {

    for(let i = 0; i < this.whoIsChecked.length; i++) {
      this.whoIsChecked[i] = true;
    }
    this.allStatus = true;
    this.oneStatus = false;
  }

  public getMyValue(checkboxid:number):boolean {
    return this.whoIsChecked[checkboxid];
  }

  public getAllValues() {
    for(let i = 0; i < this.whoIsChecked.length; i++) {
      if(this.whoIsChecked[i] == false){
        return false;
      }
    }
    return true;
  }

  public clickItem(index:number, item:any){
    this.whoIsChecked[index] = !this.whoIsChecked[index];
    if(this.whoIsChecked[index] == true) {
      this.filter = item.title;
      this.allStatus = false;
      this.oneStatus = true;
    } else {
      this.allStatus = true;
      this.oneStatus = false;
    }
    this.eventDispatcher.broadcast(StatusFilterComponent.SELECTED_STATUS_FILTER, this.filter);
    Logger.log(this.filter);

  }

}
