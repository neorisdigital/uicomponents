import { Component }          from '@angular/core';
import { UserDTO }            from '../../DTO/userDTO';
import { SessionService}      from '../../services/session.service';
import { TranslationService } from '../../services/translation.service';
import { Router }             from '@angular/router';

@Component({
    selector: 'cemex-header',
    templateUrl: './header.component.html',
    providers: [TranslationService],
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  user: UserDTO = new UserDTO;
  flag: string = '/vendor/images/usa_flag.svg'; 

  enflagged: boolean = true;
  esflagged: boolean = false;

  constructor(private seshService: SessionService, 
              private t: TranslationService,
              private router: Router) {
                 setTimeout(() => {
                  this.user = this.seshService.getSession();
                  this.changeLanguage(t.pt('lang'));
                }, 2000);
               }

    getUserInfo() {
      this.user = this.seshService.getSession() as UserDTO;
    }

    changeLanguage(lang:any) {
      this.t.lang(lang);
      if (lang == 'es') {
        this.flag = '/vendor/images/mex_flag.svg';
        this.esflagged = true;
        this.enflagged = false;
      } else {
        this.flag = '/vendor/images/usa_flag.svg';
        this.enflagged = true;
        this.esflagged = false;
      }
    }

    logout() {
      this.seshService.logout();
      location.reload();
      this.router.navigate(['login']);
    }

}