import { Component, Input, Output }  from '@angular/core'
import { TranslationService } from '../../services/translation.service';

@Component({
  selector:     'search-field',
  templateUrl:  './search-field.component.html',
  providers:    [ TranslationService ],
  styleUrls:    ['./search-field.component.scss']
})

export class SearchFieldComponent {
    constructor(private t: TranslationService) {}
}