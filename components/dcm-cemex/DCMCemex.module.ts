import { NgModule } from '@angular/core';
// Components
import { MdButtonModule } from './../md-button/md-button.module';

const DCM_MODULES = [
    MdButtonModule,
];

@NgModule({
    imports: [
        MdButtonModule.forRoot(),
    ],
    exports: DCM_MODULES,
})
export class DCMCemexRootModule{}

@NgModule({
    imports: DCM_MODULES,
    exports: DCM_MODULES,
})
export class DCMCemexModule{
    static forRoot(){
        return {
            ngModule: DCMCemexRootModule,
        }
    }
}