import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdInputContainerComponent } from './index';
import {
    MdIcon,
    MdIconStyler,
    MdIconExtraLargeStyler,
    MdIconLargeStyler,
    MdIconMediumStyler,
    MdIconSmallStyler,
    MdIconExtraSmallStyler } from './../md-icon/';

@Component({
    selector: 'demo-md-input-container',
    templateUrl: './demo.component.html'
})
export class DemoMdInputContainerComponent implements OnInit{
  
    private _paymentForm: FormGroup;
    private _formBuilder: FormBuilder;    

    constructor( formBuilder: FormBuilder ){
        this._formBuilder = formBuilder;
      
    }

    ngOnInit(){
        this._paymentForm = this._formBuilder.group({
            paymentAmount: [ '', Validators.required ],
        });
    }

    processFinished():void{
        alert( "Wizard finished the process" );
    }
}