import { Component, ViewEncapsulation, Input, OnInit } from '@angular/core';

@Component({
    selector: 'md-input-container',
    templateUrl: './md-input-container.component.html',
    styleUrls: [ './md-input-container.component.scss' ],
    encapsulation: ViewEncapsulation.None
})
export class MdInputContainerComponent implements OnInit{
    @Input( 'icon-float' ) private _iconFloat: boolean;
    @Input( 'form-control-name' ) private _formControlName: Object;
    private _formControlExists = true;

    ngOnInit(): void{
        if( this._formControlName === undefined || this._formControlName === null ){
            this._formControlExists = false;
        }
    }
}