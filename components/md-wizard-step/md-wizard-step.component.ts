import { Component, Input } from '@angular/core';

@Component({
    selector: 'md-wizard-step',
    templateUrl: './md-wizard-step.component.html',
    styleUrls: [ './md-wizard-step.component.scss' ]
})
export class MdWizardStepComponent{
    @Input( 'step-title' ) title: string;
    @Input( 'form-title' ) private _formTitle: string;
    @Input( 'form-subtitle' ) private _formSubtitle: string;
    @Input() active = false;

    constructor(){
    }

    wizardClicked(){
        console.log( 'md-wizard-step clicked' );
    }

}