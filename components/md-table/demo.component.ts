import { Component, OnInit } from '@angular/core';
import { MdTableComponent } from '../../../uicomponents/components/md-table/md-table.component';
import { MdTableItemComponent } from '../../../uicomponents/components/md-table-item-invoice/md-table-item-invoice.component';

import { InvoiceService } from '../../../uicomponents/services/invoice.service';

import DocumentsDTO from '../../DTO/documentsDTO';

// import { Http } from '@angular/http';



@Component({
    selector: 'DemoMdTable',
    templateUrl: './demo.component.html',
    providers: [InvoiceService]
})


export class DemoMdTableComponent implements OnInit {



    public tblhead: string[];
    public Urls: string;
    public documentsList: Promise<DocumentsDTO[]>;

    public status: boolean = true;
    public chck: string = "inputcheckbox";
    // public arrayclass: any =
    // {
    //     padding: 0,
    //     width: "8px",

    //     // "text-align": "center"
    // };
    _invoiceService: InvoiceService;



    // constructor(private http: Http) {

    //     this._http = http;
    constructor(invoiceService: InvoiceService) {
        this._invoiceService = invoiceService;
    }
 

    listinvoice: any;

    ngOnInit() {


        this.tblhead = ["Reference", "Jobsite", "Contract", "PO", "Type", "Created", "Due Date", "Total", "Balance Due"];
        this.documentsList = this._invoiceService.getAllInvoices();


    }



}

