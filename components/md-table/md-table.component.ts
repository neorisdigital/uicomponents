import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { MdTableItemComponent } from './../md-table-item-invoice/md-table-item-invoice.component';

@Component({
    selector: 'md-table',
    templateUrl: './md-table.component.html',
    styleUrls: ['./md-table.component.scss'],

})
export class MdTableComponent implements OnChanges {


    @Input() tblhead: string[];
    @Input() tblDet: string[];
    @Input() thstatuscolor: boolean;
    @Input() stylethstatuscolor: string;
    @Input() thstatusObjarraycss: any;
    @Input() thcontrolhtml: string;

  

    ngOnChanges(changes) {

        this.tblhead == null ? this.tblhead : null;
        this.tblDet == null ? this.tblDet : null;
        this.thcontrolhtml == null ? this.thcontrolhtml : null;


    }



    constructor() { }




    testList(): Array<any> {
        return [
            {
                id: 1, Reference: '2189312980849', objReference: { Invoice: "Invoice #      9432671687", StatusReference: "" }, Jobsite: '23490194023', objJobSite: { Summary: "Chicago with 40 char..." }, Contract: '2918289439', Po: '77652894382', Type: 'I', Created: '30/1/17', DueDay: '15/5/17', Total: '$20,000.00', BalanceDue: '$15,000.00',
                statusbalance: 'Primary', objBalanceDue: { Overdue: "5 Days Overdue", Discount: "$50.00 Discount" }
            },
            {
                id: 2, Reference: '8764765765476', objReference: { Invoice: "Invoice #      8764647799", StatusReference: "Paymant in Progress" }, Jobsite: '23490194023', objJobSite: { Summary: "Chicago with 40 char..." }, Contract: '2918289439', Po: '77652894382', Type: 'dn', Created: '3/2/17', DueDay: '15/5/17', Total: '$30,000.00', BalanceDue: '$5,000.00',
                statusbalance: 'Secudary', objBalanceDue: { Overdue: "3 Days Overdue", Discount: "" }
            },


            {
                id: 3, Reference: '2189312980849', objReference: { Invoice: "Invoice #      9432671687", StatusReference: "" }, Jobsite: '23490194023', objJobSite: { Summary: "Chicago with 40 char..." }, Contract: '2918289439', Po: '77652894382', Type: 'I', Created: '13/1/17', DueDay: '21/5/17', Total: '$25,000.00', BalanceDue: '$10,000.00',
                statusbalance: 'Secudary', objBalanceDue: { Overdue: "2 Days to Due", Discount: "$500.00 Discount" }
            },

            {
                id: 4, Reference: '2189312980849', objReference: { Invoice: "Invoice #      9432671687", StatusReference: "" }, Jobsite: '23490194023', objJobSite: { Summary: "Chicago with 40 char..." }, Contract: '2918289439', Po: '77652894382', Type: 'I', Created: '30/1/17', DueDay: '15/5/17', Total: '$65,000.00', BalanceDue: '$35,000.00',
                statusbalance: 'Third', objBalanceDue: { Overdue: "27 Days to Due", Discount: "$1,500.00 Discount" }
            },

            {
                id: 5, Reference: '2189312980849', objReference: { Invoice: "Invoice #      9432671687", StatusReference: "" }, Jobsite: '23490194023', objJobSite: { Summary: "Chicago with 40 char..." }, Contract: '2918289439', Po: '77652894382', Type: 'I', Created: '19/1/17', DueDay: '30/5/17', Total: '$20,000.00', BalanceDue: '$20,000.00',
                statusbalance: 'Third', objBalanceDue: { Overdue: "27 Days to Due", Discount: "$500.00 Discount" }
            }



        ];
    }
}




interface documents {

    // referenceNumber: string;
    // folioDocument: number;
    // jobSiteCode: number;
    // jobSiteDesc: string;
    // contract: number;
    // totalContracts: number;
    // purchaseOrder: number;
    documentTypeCode: string;
    // createDate: string;
    // dueDate: string;
    // amount: number;
    // balance: number;
    // discount: string;

}