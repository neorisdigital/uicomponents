import { Component, Input, ViewChild, ContentChildren, QueryList, OnInit, OnChanges } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
@Component({
    selector: 'md-table-item',
    templateUrl: './md-table-item-invoice.component.html',
    styleUrls: ['./md-table-item-invoice.component.scss'],

})
export class MdTableItemComponent {



    @Input() tblItems: any;


    constructor() { }



    getTdClass(cssname: string, status: number) {
        let cssClasses;



        if (cssname == 'ColorColummnStatus' && status == 1) {
            cssClasses = "ColorColummnStatus1";
        }
        else if (cssname == 'ColorColummnStatus' && status == 2) {
            cssClasses = "ColorColummnStatus2";
        }

        else if (cssname == 'ColorColummnStatus' && status == 3) {
            cssClasses = "ColorColummnStatus3";
        }
        else if (cssname == 'formatReferenceLink') {
            cssClasses = "formatReferenceLink";
        }
        else if (cssname == 'formatReference') {
            cssClasses = "formatReference";
        }

        else if (cssname == 'formatReferenceFolioDocument') {
            cssClasses = "formatReferenceFolioDocument";
        }
        else if (cssname == 'FormatJobs') {
            cssClasses = "FormatJobs";
        }
        else if (cssname == 'FormatJobsChild') {
            cssClasses = "FormatJobsChild";
        }
        else if (cssname == 'FotmatPO') {
            cssClasses = "FotmatPO";
        }
        else if (cssname == 'FormatContract') {
            cssClasses = "FormatContract";
        }
        else if (cssname == 'FormatContractChild') {
            cssClasses = "FormatContractChild";
        }
        else if (cssname == 'FormatColumns') {
            cssClasses = "FormatColumns";
        }
        else if (cssname == 'FormatColumnsChild') {
            cssClasses = "FormatColumnsChild";
        }

        else if (cssname == 'Formattype') {
            cssClasses = "Formattype";
        }

        else if (cssname == 'FormatCreate') {
            cssClasses = "FormatCreate";
        }

        else if (cssname == 'FormatDueDate' && status == 1) {
            cssClasses = "FormatDueDate1";
        }
        else if (cssname == 'FormatDueDate' && status == 2) {
            cssClasses = "FormatDueDate2";
        }

        else if (cssname == 'FormatDueDate' && status == 3) {
            cssClasses = "FormatDueDate3";
        }

        else if (cssname == 'Formattotal') {
            cssClasses = "Formattotal";
        }
        else if (cssname == 'formatBalanceDue' && status == 1) {
            cssClasses = "formatBalanceDue1";
        }
        else if (cssname == 'formatBalanceDue' && status == 2) {
            cssClasses = "formatBalanceDue2";
        }
        else if (cssname == 'formatBalanceDue' && status == 3) {
            cssClasses = "formatBalanceDue3";
        }


        else if (cssname == 'formatBalanceChild' && status == 1) {
            cssClasses = "formatBalanceChild1";
        }
        else if (cssname == 'formatBalanceChild' && status == 2) {
            cssClasses = "formatBalanceChild2";
        }
        else if (cssname == 'formatBalanceChild' && status == 3) {
            cssClasses = "formatBalanceChild2";
        }

        else if (cssname == 'formatBalanceDiscount') {
            cssClasses = "formatBalanceDiscount";
        }
        else { 

            cssClasses = ""
        }
        return cssClasses;
    }

    mouselink: number = -1;
    mouseid: number = -1;
    over(event, index: number) {

        this.mouseid = index;
        console.log("Mouseover" + index);
    }


    leave(event, index: number) {

        this.mouseid = index;
        console.log("mouseleave" + index);
    }



}
























