import { Component } from '@angular/core';
import {SessionService} from '../../services/session.service';
import {HttpCemex} from '../../services/http.service';
import {CredentialsConstants} from '../../constants/credentials.constants';
import {Broadcaster} from '../../events/broadcaster.event';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';

@Component({
    selector: 'DemoTranslationComponent',
    templateUrl: './demo.component.html',
    providers: [SessionService,HttpCemex,Broadcaster,TranslationService]
})
export class DemoTranslationComponent {

}