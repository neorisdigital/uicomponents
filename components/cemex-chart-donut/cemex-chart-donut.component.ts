import { Component,Input,OnChanges } from '@angular/core';
import {Logger} from '../../services/logger.service';
// webpack html imports
let template = require('./cemex-chart-donut.component.html');

@Component({
  selector: 'cemex-chart-donut',
  template: template,
  styleUrls: ['./cemex-chart-donut.component.scss']
})
export class CemexChartDonutComponent implements OnChanges{
  @Input() chartData: any[];
  // Doughnut
  public doughnutChartLabels:string[] = [];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';
  private doughnutChartColors: any[] = [{ backgroundColor: ["#b8436d", "#00d9f9", "#a4c73c", "#a4add3"] }];
  private chartOptions:any={rotation: 1 * Math.PI,circumference: 1 * Math.PI};


  constructor(){
    if(this.chartData==null || this.chartData==undefined){
      this.chartData=[];
    }
    if(this.chartData.length==0){
      this.chartData=[{label:"label num 1",value:300},{label:"label num 2",value:450}]
    }
    this.getValuesForChart();
    this.getLabelsForChart();
  }

  ngOnChanges(changes) {
      this.getLabelsForChart();
      this.getValuesForChart();
  }

  public getLabelsForChart():void{
    this.doughnutChartLabels=[];
    for(let item of this.chartData){
      this.doughnutChartLabels.push(item.label);
    }
  }
  public getValuesForChart():void{
    this.doughnutChartData=[];
    for(let item of this.chartData){
      this.doughnutChartData.push(item.value);
    }
  }
  private getLabelColor(index:number):string{
    //Logger.log(this.doughnutChartColors[0].backgroundColor[index]);
    //return this.doughnutChartColors[0].backgroundColor[index];
     if(index>3){
      this.doughnutChartColors[0].backgroundColor[index%4];
    }
    return this.doughnutChartColors[0].backgroundColor[index];
  }

  // events
  public chartClicked(e:any):void {
    // console.log(e);
  }
  

  public chartHovered(e:any):void {
    // console.log(e);
  }
}