import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CemexButtonComponent, ButtonSize, ButtonType } from '../cemex-button/cemex-button.component'

@Component({
    selector: 'cemex-modal',
    templateUrl: './cemex-modal.component.html',
    styleUrls: ['./cemex-modal.component.scss']
})
export class CemexModalComponent {
    @Input() buttontag: string;
    @Input() body: any;
    @Input() top: number;
    @Input() bottom: number;
    @Input() right: number;
    @Input() left: number;

    constructor() {
    this.buttontag = "Tag"
    this.body = "Content"
    this.top = 0
    this.bottom = 0
    this.right = 0
    this.left = 0
  }
}