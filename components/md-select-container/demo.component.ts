import { Component } from '@angular/core';
import { MdSelectContainerComponent } from './index';
import {
    MdIcon,
    MdIconStyler,
    MdIconExtraLargeStyler,
    MdIconLargeStyler,
    MdIconMediumStyler,
    MdIconSmallStyler,
    MdIconExtraSmallStyler } from './../md-icon/';

@Component({
    selector: 'demo-md-select',
    templateUrl: './demo.component.html'
})
export class DemoMdSelectContainerComponent {
}