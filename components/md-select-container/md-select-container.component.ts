import { CommonModule } from '@angular/common';
import { Component, NgModule, ViewEncapsulation, Input, OnInit } from '@angular/core';

@Component({
    selector: 'md-select-container',
    templateUrl: './md-select-container.component.html',
    styleUrls: [ './md-select-container.component.scss' ],
    encapsulation: ViewEncapsulation.None
})
export class MdSelectContainerComponent implements OnInit{
    @Input( 'form-control-name' ) private _formControlName: Object;

    ngOnInit(){
        if( this._formControlName === undefined ){
            this._formControlName = {
                override: true
            };
        }
    }
}