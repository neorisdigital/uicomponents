import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TranslationService }             from '../../services/translation.service';
import { Logger }                         from '../../services/logger.service';
import { Broadcaster }                    from '../../../uicomponents/events/broadcaster.event';

import JobsiteDTO from '../../DTO/jobsiteDTO';

@Component({
    selector: 'jobsite-filter',
    templateUrl: './jobsite-filter.component.html',
    styleUrls: ['./jobsite-filter.component.scss'],
    providers: [TranslationService]
})

export class JobsiteFilterComponent {
    @Input() firstTab: any;
    @Input() secondTab: any;
    @Input() tabWidth: any;
    @Input() filter1: any;
    @Input() filter2: any;

    constructor(private transaltor: TranslationService, private ev: Broadcaster) {
        this.firstTab = "First Tab";
        this.secondTab = "Second Tab";
        this.tabWidth = "";
        this.filter1 = "filter1";
        this.filter2 = "filter2";
    }
    static SELECTED_TAB = "SELECTED_TAB_ORDERDT";

     public toggleFilter(tab:any){
         console.log('consoleee_>1', tab)
        this.ev.broadcast(JobsiteFilterComponent.SELECTED_TAB, tab)
    }
    
    public toggleFilter2(tab:any){
        console.log('consoleee_>2', tab)
        this.ev.broadcast(JobsiteFilterComponent.SELECTED_TAB, tab)
    }
}