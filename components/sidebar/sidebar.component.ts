import { Component, Input, OnChanges } from '@angular/core';

import { Logger } from '../../services/logger.service';
import { JobsiteService } from '../../services/jobsite.service';
import { Broadcaster } from '../../events/broadcaster.event';

// Providers needed
import { TranslationService } from '../../services/translation.service';
import { SidebarEvents } from '../../events/sidebar.event';

// Models, just a comment
import JobsiteDTO from '../../DTO/jobsiteDTO';
import CustomerDTO from '../../DTO/customerDTO';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    providers: [TranslationService]
})
export class SidebarComponent implements OnChanges {
    @Input() jobsites: JobsiteDTO[] = new Array();
    @Input() customers: CustomerDTO[] = new Array();
    @Input() loading: boolean = false;

    // Delete this later
    // ============================================
    public static JOBSITE_SELECTED: string = "JOBSITE_SELECTED"
    public static JOBSITE_SELECTED_DTO: string = "JOBSITE_SELECTED_DTO"
    public static CUSTOMER_SELECTED: string = "CUSTOMER_SELECTED"
    public static CUSTOMER_SUMMARY:string="CUSTOMER_SUMMARY"
    // ============================================

    private mainDropOpened: boolean = false;
    public selectedCustomer: CustomerDTO;
    public selectedJobsite: JobsiteDTO = null;

    public showFavorites: boolean = true;
    public showActives: boolean = false;
    public showInactives:boolean = false;
    public favesExist: boolean = true;

    constructor(private eventDispatcher: Broadcaster,
                private t: TranslationService,
                private sidebarListener: SidebarEvents) {}

    ngOnChanges(changes: any) {
        if (changes.customers) {
            this.selectCustomer(0);
        }
        if (changes.jobsites && this.jobsites.length > 0) {
            if (!this.selectedJobsite) {
                this.selectJobsite(this.jobsites[0]);
            }
        }
    }

    // Controller logic
    // ============================================================
    private selectCustomer(index: number) {
        this.selectedCustomer = this.customers[index];
        this.sidebarListener.selectCustomer(this.selectedCustomer);
    }

    private selectJobsite(jobsite: JobsiteDTO) {
        this.selectedJobsite = jobsite;
        this.sidebarListener.selectJobsite(jobsite);

        // Delete this later
        // ============================================
        this.eventDispatcher.broadcast(SidebarComponent.JOBSITE_SELECTED, jobsite.jobsiteId);
        this.eventDispatcher.broadcast(SidebarComponent.JOBSITE_SELECTED_DTO, jobsite);
        // ============================================
    }

    private gotoGlobalSummary(){
        this.eventDispatcher.broadcast(SidebarComponent.CUSTOMER_SUMMARY);
    }

    private sameJobsiteId(jobsite: JobsiteDTO): boolean {
        return this.selectedJobsite.jobsiteId == jobsite.jobsiteId;
    }

    // View logic
    // ============================================================
    private toogleMainDropdown() {
        this.mainDropOpened = !this.mainDropOpened;
    }

    private confirmMainDropdown() {
        this.mainDropOpened = false;
    }

    private mainDropdownStyle() {
        let style = "dropdown";
        if (this.mainDropOpened) {
            style += " open";
        }
        return style;
    }

    public getJobsitesQty(): number {
        if (this.showFavorites) {
            var count = 0;
            this.jobsites.forEach(function(jobsite) {
                count += jobsite.favoriteStatus == 'Y' ? 1 : 0;
            });
            this.favesExist = true;
            return count; 
        }
        else {
            this.favesExist = false;
            return this.jobsites.length;
        }
    }

    isSelected(jobsite: JobsiteDTO): boolean {
        if (this.selectedJobsite) {
            return this.selectedJobsite.jobsiteId == jobsite.jobsiteId;
        }
        else
            return false;
    }

    private onTab(tab: any){
        if(tab == "favorites") {
            this.showFavorites = true;
            this.showActives = false;
            this.showInactives = false;
        } else if(tab == "active") {
            this.showFavorites = false;
            this.showActives = true;
            this.showInactives = false;
        } else {
            this.showFavorites = false;
            this.showActives = false;
            this.showInactives = true;
        }
    }
}
