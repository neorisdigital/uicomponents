import { Component, EventEmitter, Output, Input } from '@angular/core'
import { Broadcaster } from '../../events/broadcaster.event';
import { Logger } from '../../services/logger.service';
import { TranslationService } from '../../services/translation.service';
import JobsiteDTO from '../../DTO/jobsiteDTO';
import OrderDTO from '../../DTO/orderDTO';
let _ = require('underscore');
@Component({
  selector: 'pod-filter',
  templateUrl: './pod-filter.component.html',
  providers: [Broadcaster, TranslationService],
  styleUrls: ['./pod-filter.component.scss']
})

export class PodFilterComponent {
  allSelected: boolean = true;
  pods: any = [];
  podsList:any = [];
  podsText:string = "All Point Of Delivery";
  @Input() orders: any = [];
  static CHANGE_POD_FILTER = "CHANGE_POD_FILTER";
  constructor(private eventDispatcher: Broadcaster,
    private t: TranslationService) {
      
  }

  ngOnChanges(changes: any) {
     this.getPods();
  }

  public getPods():void{
      if(this.orders){
        this.pods = _.uniq(_.pluck(this.orders.Orders, "pointofDelivery"), function(point) {
                        return point.orderPointOfDeliveryId;
                      });
        //to fix empty properties
        for(let pod of this.pods){
          if(!pod.pointOfDeliveryCode){
            pod.pointOfDeliveryCode = "0000";
          }
          if(!pod.pointOfDeliveryDesc){
            pod.pointOfDeliveryDesc = "No Point of Delivery";
          }
          pod.selected = true;
          this.podsList.push(pod.orderPointOfDeliveryId);
        }

      }
  }


  //for POD
  public checkAll():void{
    this.podsList = [];
    this.allSelected = !this.allSelected;
    this.podsText = "All Point Of Delivery";
    this.changeAll(this.allSelected);
    this.eventDispatcher.broadcast(PodFilterComponent.CHANGE_POD_FILTER, this.podsList);
  }

  public changeAll(flag):void{
      for(let pod of this.pods){
        pod.selected = flag;
        this.updatePodList(pod);
      }
  }

  public selectItem(pod):void{
    this.allSelected = false;
    pod.selected = !pod.selected;
    this.updatePodList(pod);
    if(this.podsList.length == 1){
         let item =  _.find(this.pods, function(item){ return item.orderPointOfDeliveryId == this.podsList[0]; }, this);
         this.podsText = item.pointOfDeliveryCode + "-" + item.pointOfDeliveryDesc;
    }
    else{
      this.podsText = this.podsList.length + " Points of Delivery";
    }
    this.eventDispatcher.broadcast(PodFilterComponent.CHANGE_POD_FILTER, this.podsList);
  }

  public updatePodList(pod):void{
     if(pod.selected){
      this.podsList.push(pod.orderPointOfDeliveryId);
    }
    else{
      let index = this.podsList.indexOf(pod.orderPointOfDeliveryId);
      if(index != -1){
        this.podsList.splice(index, 1)
      }
    }
  }

}
