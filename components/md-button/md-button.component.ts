import { Component, NgModule, ViewEncapsulation, Input, ElementRef, Renderer } from '@angular/core';

@Component({
    selector: 'button[md-button]',
    templateUrl: './md-button.component.html',
    styleUrls: [ './md-button.component.scss' ],
    encapsulation: ViewEncapsulation.None,
})
export class MdButton{
    private _color: string;
    private _renderer: Renderer;
    private _elementRef: ElementRef;

    constructor( renderer: Renderer, elementRef: ElementRef ){
        this._renderer = renderer;
        this._elementRef = elementRef;
    }

    @Input()
    get color(): string{
        return this._color;
    }
    set color( value: string ){
        this._updateColor( value );
    }

    private _updateColor( newColor: string ){
        this._setElementColor( this._color, false );
        this._setElementColor( newColor, true );
        this._color = newColor;
    }

    private _setElementColor( color: string, isAdd: boolean ){
        if( color !== null && color !== '' ){
            this._renderer.setElementClass( this._getHostElement(), `md-${color}`, isAdd );
        }
    }

    private _getHostElement(){
        return this._elementRef.nativeElement;
    }
}