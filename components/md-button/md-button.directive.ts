import { Directive } from '@angular/core';

@Directive({
    selector: 'button[md-button]',
    host:{
        '[class.md-button]': 'true',
    }
})
export class MdButtonStyler{
    constructor(){
    }
}