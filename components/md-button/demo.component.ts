import { Component } from '@angular/core';
import { MdButton, MdButtonStyler } from './index';
import {
    MdIcon,
    MdIconStyler,
    MdIconExtraLargeStyler,
    MdIconLargeStyler,
    MdIconMediumStyler,
    MdIconSmallStyler,
    MdIconExtraSmallStyler } from './../md-icon/';

@Component({
    selector: 'demo-md-button',
    templateUrl: './demo.component.html'
})
export class DemoMdButtonComponent {
}