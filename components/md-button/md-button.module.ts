import { NgModule } from '@angular/core';
import { MdButton } from './md-button.component';
import { MdButtonStyler } from './md-button.directive';

@NgModule({
    exports: [
        MdButton,
        MdButtonStyler,
    ],
    declarations: [
        MdButton,
        MdButtonStyler
    ]
})
export class MdButtonModule{
    static forRoot(){
        return {
            ngModule: MdButtonModule,
            providers: []
        }
    }
}