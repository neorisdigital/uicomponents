import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'md-radio-button',
    templateUrl: './md-radio-button.component.html',
    styleUrls: [ './md-radio-button.component.scss' ],
    encapsulation: ViewEncapsulation.None
})
export class MdRadioButtonComponent{
    @Input( 'image-src' ) private _imageSrc: String;
}