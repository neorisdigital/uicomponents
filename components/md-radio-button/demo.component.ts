import { Component, OnInit } from '@angular/core';
import { MdRadioButtonComponent } from './index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//ReactiveFormsModule is required in app.module.ts

@Component({
    selector: 'demo-md-radio-button',
    templateUrl: './demo.component.html'
})
export class DemoMdRadioButtonComponent implements OnInit {

    private _cardForm: FormGroup;  
    private _paymentForm: FormGroup;
    private _formBuilder: FormBuilder;  

    
    constructor( formBuilder: FormBuilder ){
        this._formBuilder = formBuilder;
    }

    ngOnInit(){
        this._paymentForm = this._formBuilder.group({
            bank: [ '', Validators.required ]
        });

        this._cardForm = this._formBuilder.group({
            card: [ '', Validators.required ]
        });
    }

    processFinished():void{
        alert( "Wizard finished the process" );
    }

}