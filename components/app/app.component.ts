import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    // Globals config
    useLayout: boolean = true;

    constructor(router: Router) {
        router.events.subscribe((val) => {
            if (val.url == "/login") { this.useLayout = false; }
            else { this.useLayout = true; }
        });
    }
}
