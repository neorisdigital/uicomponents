import {
    Component,
    ViewEncapsulation,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ContentChildren,
    QueryList,
    AfterContentInit } from '@angular/core';
import { MdWizardStepComponent } from './../md-wizard-step/md-wizard-step.component';
import { ModalDirective } from 'ng2-bootstrap';

@Component({
    selector: 'md-wizard',
    templateUrl: './md-wizard.component.html',
    styleUrls: ['./md-wizard.component.scss'],
})
export class MdWizardComponent implements AfterContentInit{
    @ContentChildren( MdWizardStepComponent ) wizardSteps: QueryList<MdWizardStepComponent>;
    @ViewChild( 'staticModal' ) private _wizardModal: ModalDirective;
    @Input( 'form-title' ) private _formTitle: string;
    private _currentStep: number;
    @Output() public finished: EventEmitter<boolean>;
    @Output() public canceled: EventEmitter<boolean>;

    constructor(){
        this.finished = new EventEmitter<boolean>();
        this.canceled = new EventEmitter<boolean>();
    }

    ngAfterContentInit(): void{
        this.wizardSteps.first.active = true;
        this._currentStep = 0;
    }

    public open(): void{
        this._wizardModal.show();
    }

    public next(): void{
        let stepsArray = this.wizardSteps.toArray();
        stepsArray[ this._currentStep ].active = false;
        this._currentStep += 1;
        if( this._currentStep < stepsArray.length ){
            stepsArray[ this._currentStep ].active = true;
        }
        else{
            this.finish();
        }
    }

    public finish(): void{
        this._currentStep = 0;
        let stepsArray = this.wizardSteps.toArray();
        stepsArray.forEach( step => {
            step.active = false;
        });
        this.wizardSteps.first.active = true;
        this._wizardModal.hide();
        this.finished.emit( true );
    }

    public cancel(): void{
        this._currentStep = 0;
        let stepsArray = this.wizardSteps.toArray();
        stepsArray.forEach( step => {
            step.active = false;
        });
        this.wizardSteps.first.active = true;
        this._wizardModal.hide();
        this.canceled.emit( true );
    }
}
