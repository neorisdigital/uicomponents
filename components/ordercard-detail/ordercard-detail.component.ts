import {Component, Injectable, NgZone, EventEmitter, Output, Input,OnInit } from '@angular/core';
import { Broadcaster }                     from '../../events/broadcaster.event';
import   JobsiteDTO                          from '../../DTO/jobsiteDTO';
import   CustomerDTO                         from '../../DTO/customerDTO';
import   orderDTO                            from '../../DTO/orderDTO'
import { OrderService }                    from '../../services/order.service';
import { TranslationService }              from '../../services/translation.service';
import { ApiService }                      from '../../services/http.api.service';
import { FilterComponent}                  from '../../components/filter/filter.component';
import { SidebarComponent}                 from '../../components/sidebar/sidebar.component';
import { orderCardComponent }			   from '../ordercard/ordercard.component';
// import  GeneralHelper                      from '../../helpers/newhelpers';
import { Router } from '@angular/router';

// webpack html imports
let template = require('./ordercard-detail.component.html');

@Component({
  selector: 'order-card-detail',
  template: template,
  providers:[OrderService, FilterComponent],
  styleUrls: ['./ordercard-detail.component.scss']
})

@Injectable()
export class CardDetailComponent implements OnInit{
  public static ORDERS_IN_CARDS:string="orders_in_cards";
  public orders:any=[];
  public ordersNew:any=[];
  public orderLenght:number;
  public canceled:any;
  public filter1:string;
  public filter2:string;
  public DOP:any;
  public jobsites:any;
  public status:any=[];
  public filter3:boolean;
  public OI:any=[]; 
  public PI:any;
  public FU:any;
  public ROI:any; 
  public static lastSelectedOrder:orderDTO=null;


  constructor(private ev: Broadcaster, private api: ApiService, private ord: OrderService,private router:Router, private t:TranslationService) {
    // this.PI = 907; 
    this.filter1 ='';
    this.filter2 ='';
    this.filter3 = true; 

            this.FU = orderCardComponent.lastSelectedOrder;
            this.PI = this.FU.orderId;
            this.ROI = this.FU.orderItem;

              this.ord.getTicketbyId(Number(this.PI)).subscribe(
                res => { this.OI = res, this.getCards(this.OI,this.ROI)},
                err => {console.error('Error', err)}
              )
    this.status = [ { title: 'Active', color:'NST' }, { title: 'In Progress', color:'STR' }, { title: 'To be Confirmed', color:'WCL' }, { title: 'On Hold', color:'OHD' }, { title: 'Cancelled', color:'CNC' }, { title: 'Completed', color:'CMP' }, { title: 'Payment Hold', color:'PHD' } ]; 

  }

  ngOnInit(){
  	
  }

  public changeto(){
      this.filter3 = true;
  }

  public changeIn(){
      this.filter3 = false; 
  }


  public getCards(data:any, data2:any){
       for (let item of data) {
        for (let ticket of data2) {
          if(item.loadNumber == ticket.loadNumber) {
            for (var i = this.ROI.length - 1; i >= 0; i--) {
              this.ROI[i].ticketCode = item.ticketCode
            }
          }
        }

       }
  }

}

