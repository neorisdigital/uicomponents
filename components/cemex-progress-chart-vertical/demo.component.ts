import { Component } from '@angular/core';
import {HttpCemex} from '../../services/http.service';
import {CredentialsConstants} from '../../constants/credentials.constants';
import {Broadcaster} from '../../events/broadcaster.event';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';




@Component({
    selector: 'demo-cemex-progress-chart',
    templateUrl: './demo.component.html',
    providers: [HttpCemex,Broadcaster,TranslationService]
})

export class DemoCemexProgressChartComponent{
   constructor(private eventDispatcher:Broadcaster, private t:TranslationService) {}
}