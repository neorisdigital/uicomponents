import { Component, Input, Output, EventEmitter } from '@angular/core';

export enum ButtonType {
    primary = 1,
    secondary = 2,
    disabled = 3
}

export enum ButtonSize {
    normal = 1,
    medium = 2,
    small = 3
}

@Component({
  selector: 'cemex-button',
  templateUrl: './cemex-button.component.html',
  styleUrls: ['./cemex-button.component.css'],
})
export class CemexButtonComponent {
  @Input() title: string;
  @Input() type: ButtonType;
  @Input() size: ButtonSize;

  constructor() {
    this.title = "Send"
    this.type = ButtonType.primary
    this.size = ButtonSize.normal
  }

  getStyle() {
    return this.getTypeClass() + " " + this.getSizeClass()
  }

  getTypeClass() {
    if (this.type == ButtonType.primary) {
      return "primary"
    }
    else if (this.type == ButtonType.secondary) {
      return "secondary"
    }
    else if (this.type == ButtonType.disabled) {
      return "disabled"
    }
    else {
      // defualt case
      return ""
    }
  }

  getSizeClass() {
    if (this.size == ButtonSize.normal) {
      return "normal"
    }
    else if (this.size == ButtonSize.medium) {
      return "medium"
    }
    else if (this.size == ButtonSize.small) {
      return "small"
    }
    else {
      // defualt case
      return ""
    }
  }
}