import { Component } from '@angular/core';
import { CemexButtonComponent, ButtonSize, ButtonType } from '../cemex-button/cemex-button.component'
@Component({
    selector: 'demo-cemex-button',
    templateUrl: './demo.component.html'
})
export class DemoCemexButtonComponent {
     // Button types
    buttonPrimary: ButtonType = ButtonType.primary;
    buttonSecondary: ButtonType = ButtonType.secondary;
    buttonDisabled: ButtonType = ButtonType.disabled;

    // Button sizes
    buttonNormal: ButtonSize = ButtonSize.normal;
    buttonMedium: ButtonSize = ButtonSize.medium;
    buttonSmall: ButtonSize = ButtonSize.small;

}