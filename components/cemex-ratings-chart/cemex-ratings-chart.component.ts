import { Component,Input } from '@angular/core';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';
// webpack html imports
let template = require('./cemex-ratings-chart.component.html');

@Component({
  selector: 'cemex-ratings-chart',
  template: template,
  styleUrls: ['./cemex-ratings-chart.component.scss']
})
export class CemexRatingsChartComponent {
  @Input() chartData:any[];
  @Input() totalOrders:number;
  @Input() first_headline:string;
  @Input() second_headline:string;

  private backgroundColors:string[]=["progress-bar-success","progress-bar-info","progress-bar-warning","progress-bar-danger"];

  constructor(private t:TranslationService){
    if(this.chartData==undefined || this.chartData==null){
      this.chartData=[];
    }
    if(this.chartData.length==0){
      this.chartData=[{title:"Data 1",progress_one:20,progress_two:20,extraInfo_one:"yd/hr",extraInfo_two:"yd/hr"},{title:"Data 2",progress_one:20,progress_two:20,extraInfo_one:"yd/hr",extraInfo_two:"yd/hr"}];
    }
  }

  private getClass(index:number):string{
    if(index>3){
      this.backgroundColors[index%4];
    }
    return this.backgroundColors[index];
  }
  
}