import { Component, Injectable, NgZone, EventEmitter, Output, Input, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Broadcaster } from '../../events/broadcaster.event';
import { OrderService }                    from '../../services/order.service';
import { TranslationService }              from '../../services/translation.service';
import { ApiService }                      from '../../services/http.api.service';
import { FilterComponent }                 from '../../components/filter/filter.component';
import { PodFilterComponent }              from '../../components/pod-filter/pod-filter.component';
import { CalendarEvents }                  from '../../../uicomponents/events/calendar.event';
import { HistoricalFilterComponent }       from '../../components/historical-filter/historical-filter.component';
// Models
import JobsiteDTO                          from '../../DTO/jobsiteDTO';
import CustomerDTO                         from '../../DTO/customerDTO';
import OrderDTO                            from '../../DTO/orderDTO';


//requires
let template = require('./ordercard.component.html');
let _ = require('underscore');
let  moment = require('moment');


@Component({
  selector: 'order-card',
  template: template,
  providers:[FilterComponent, PodFilterComponent],
  styleUrls: ['./ordercards.scss']
})

@Injectable()
export class orderCardComponent implements OnInit, OnChanges {
  @Input() jobsites: any;

  public static ORDERS_IN_CARDS: string = "ORDERS_IN_CARDS";
  public orders: any = [];
  public ordersNew: any = [];
  public orderLenght: number;
  public canceled: any;
  public isRange:boolean;
  public filter3: boolean = true;

  public podFilter:any = [];
  public typeFilter:any = [];
  public statusFilter:any = [];
  public DOP: any;
  public points:any;
  public originalPoints:any;
  public historicalTitle = "Historical Orders";
  public programmedDates:any = [];
  public breakDates:any = [];
  public status: any = [];
  public currentMonth: any;

  
  public static lastSelectedOrder: OrderDTO = null;

  constructor(private ev: Broadcaster, 
              private api: ApiService,
              private router:Router, 
              private t:TranslationService,
              private calendar: CalendarEvents) {
    
    this.ev.on<any>(FilterComponent.CHANGE_TYPE_FILTER).subscribe(response => {
      this.typeFilter = response;
    });

    this.ev.on<any>(FilterComponent.CHANGE_STATUS_FILTER).subscribe(response => {
      this.statusFilter = response;
    });

    this.ev.on<any>(PodFilterComponent.CHANGE_POD_FILTER).subscribe(response => {
          this.podFilter = response;
          this.applyPodFilters(this.podFilter);
    });

     this.ev.on<any>(FilterComponent.CHANGE_CARDS_STYLE).subscribe(response => {
      this.filter3 = (response === 'cards') ? true : false;
    });

     this.ev.on<any>(HistoricalFilterComponent.CHANGE_MONTH_FILTER).subscribe(response => {
      this.currentMonth = response.monthNumber;
    });
    
  }

  ngOnInit() {
    orderCardComponent.lastSelectedOrder = null;
    this.calendar.getDates().subscribe(dates => {
        if (!dates) { return; }
        this.isRange =  dates.start.diff(dates.end, "days") != 0 ? true : false;
    });
  }

  ngOnChanges(changes: any) {
     this.getCards();
  }

  public applyPodFilters(pods):void{
    this.points = _.filter(this.originalPoints, function(point) { return _.contains(pods, point.orderPointOfDeliveryId);});
  }

  public monthFilter(date){
    return moment(date).format("MM") == this.currentMonth;
  }

  public selectedOrder(order: OrderDTO){
    orderCardComponent.lastSelectedOrder = <OrderDTO> order;
    this.ev.broadcast(Broadcaster.CHANGE_SCREEN_HOME, order);
  }

  public filterByPOD(point:any){
    let id = point.orderPointOfDeliveryId;
    let status = this.statusFilter;
    let types = this.typeFilter;
    let cards;
    if(status.length || status.length){
       cards =   _.filter(this.orders, function(order){
              return (order.pointofDelivery.orderPointOfDeliveryId == id && _.contains(status, order.orderStatusGroupCode) && _.contains(types, order.shipping));
      });
    }
    else{
      cards =  _.filter(this.orders, function(order){
              return (order.pointofDelivery.orderPointOfDeliveryId == id);
      });
    }
    point.empty = (cards.length) ? true : false;
    return cards;
  }

  public getBreakDates(orders) {
    let temporalDates = [];
    let temporalMonths = [];
    let dates = _.pluck(this.orders, 'programmedOrderDateTime');
    _.each(dates, function (value, index) {
      temporalDates.push({"date": moment(value).format("MM/DD/YYYY")});
      temporalMonths.push({ "year" : moment(value).format("YYYY"), "month": moment(value).locale('es').format("MMMM"), 'monthNumber': moment(value).locale('es').format("MM") }); 
    });

    temporalMonths = _.uniq(temporalMonths, function(date){ return date.year && date.month; });
    temporalDates = _.uniq(temporalDates, function(breakDate){ return breakDate.date; })

    
     /*  temporalDates.sort(function(a,b) {
      a = a.split('/').reverse().join('');
      b = b.split('/').reverse().join('');
      return  a < b ? 1 : a > b? -1 : 0;
    });*/

    for(let temporalDate of temporalDates){
        temporalDate.points = this.getPointsByDate(temporalDate.date);
    }
    this.programmedDates = temporalMonths.reverse();
    this.breakDates      = temporalDates.reverse();
  }

  public getPointsByDate(date){
    let orders = _.filter(this.orders, function(order) {
        return  moment(order.programmedOrderDateTime).format("MM/DD/YYYY") == date;
    })
    let points = _.uniq(_.pluck(orders, "pointofDelivery"), function(point){
                    return point.orderPointOfDeliveryId;
                  });
    for(let point of points){
         point.cards =  _.filter(this.orders, function(order){
              let resultPod = order.pointofDelivery.orderPointOfDeliveryId == point.orderPointOfDeliveryId;
              let resultDate = moment(order.programmedOrderDateTime).format("MM/DD/YYYY") == moment(date).format("MM/DD/YYYY");
              return (resultPod && resultDate);
        },point);
    }
    return points;
  }

  public getCardsByPoint(date, point){
    let data = {"date": date, "pod": point};
    let status = this.statusFilter;
    let types = this.typeFilter;
    let cards = [];
    if(status.length || status.length){
      cards =  _.filter(point.cards, function(order){
              let resultPod = order.pointofDelivery.orderPointOfDeliveryId == data.pod.orderPointOfDeliveryId;
              let resultDate = moment(order.programmedOrderDateTime).format("MM/DD/YYYY") == moment(data.date).format("MM/DD/YYYY");
              let statusFilter =  _.contains(status, order.orderStatusGroupCode);
              let typeFilter =  _.contains(types, order.shipping)
              let podFilter = _.contains(this.points, data.pod.orderPointOfDeliveryId);
              return (resultPod && resultDate && statusFilter && typeFilter);
      }, data);
    }else{
       cards =  _.filter(point.cards, function(order){
              let resultPod = order.pointofDelivery.orderPointOfDeliveryId == data.pod.orderPointOfDeliveryId;
              let resultDate = moment(order.programmedOrderDateTime).format("MM/DD/YYYY") == moment(data.date).format("MM/DD/YYYY");
              return (resultPod && resultDate);
      }, data);
    }
    point.empty = (cards.length) ? true : false;
    return cards;
  }

  public getCards() {
    
    if(!this.jobsites) { return false; };
      let im = 0;
      this.ordersNew = [];
      this.ordersNew = this.jobsites;
      this.orders = this.ordersNew.Orders;
      this.DOP = this.ordersNew.jobsiteItem;
      this.orderLenght = this.orders.length;
      
       // for(let item of this.orders){

       //        if(item.isReadyMix==true){
       //          console.log('pirro')
       //      }
       //    }
      
        for (var i = this.orders.length - 1; i >= 0; i--) {
                if(this.orders[i].shippingConditionId == 1) {
                this.orders[i].shipping = 'Delivery';
                }else{
                this.orders[i].shipping = 'Pick Up';
                }
                if(this.orders[i].orderStatusGroupCode == 'PHD') {
                this.canceled = im ++;
                }
            }
            

              for (var i = 0; i < this.orders.length; i++) {
                  let orderItems = this.orders[i].orderItem;
                  var cnl = 0,POU = 0, NTK = 0, BTG = 0, OST = 0, FIN = 0, plot = [], DLV =0;

                this.orders[i].isDeliveredTotal =0;
                this.orders[i].AllProductQty = 0;
                this.orders[i].notTicketed = 0;
                this.orders[i].batching = 0;
                this.orders[i].onSite = 0;
                this.orders[i].pouring = 0;
                this.orders[i].finished = 0;
                this.orders[i].deliveredQty=0;
                this.orders[i].shipping = 'Delivery';

               for (var t = 0; t < orderItems.length; t++) {
                this.orders[i].AllProductQty += orderItems[t].productQuantity;
                if(orderItems[t].orderDetStatusGroupCode == 'CNL') {
                cnl++;
                let ticketGroupBy = orderItems[t].ticketGroupBy;

           for (var j = ticketGroupBy.length - 1; j >= 0; j--) {
                 if(ticketGroupBy[j].isDelivered == 'True') {
                   this.orders[i].isDeliveredTotal = 1 + DLV++;
                   this.orders[i].deliveredQty += ticketGroupBy[j].deliveredQuantity;
                   
                   }
                  if(ticketGroupBy[j].ticketStatusGroupCode == 'NTK') {
                    this.orders[i].notTicketed = 1 + NTK ++;
                  }
                  if(ticketGroupBy[j].ticketStatusGroupCode == 'BTG') {
                    this.orders[i].batching = 1 + BTG ++; 
                  }
                  if(ticketGroupBy[j].ticketStatusGroupCode == 'OST') {
                    this.orders[i].onSite = 1 + OST ++;
                  }
                  if(ticketGroupBy[j].ticketStatusGroupCode == 'POU') {
                    this.orders[i].pouring = 1 + POU ++;
                  }
                  if(ticketGroupBy[j].ticketStatusGroupCode == 'FIN') {
                    this.orders[i].finished = 1 + FIN ++;
                  }
                }
            // }
             }
           }   
        }    

        if(this.isRange){
          this.getBreakDates(this.orders);          
        }
        else{
          this.points = _.uniq(_.pluck(this.orders, "pointofDelivery"), function(point) { 
                        return point.orderPointOfDeliveryId;
                       });
          this.originalPoints = _.uniq(_.pluck(this.orders, "pointofDelivery"), function(point) { 
                        return point.orderPointOfDeliveryId;
                       });
        }
  }

}