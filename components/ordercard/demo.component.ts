import {Component, Injectable, NgZone, EventEmitter, Output, Input,OnInit } from '@angular/core';
import { Broadcaster }                     from '../../events/broadcaster.event';
import { OrderService }                    from '../../services/order.service';
import { TranslationService }              from '../../services/translation.service';
import { FilterComponent}                  from '../../components/filter/filter.component';

// import  GeneralHelper                      from '../../helpers/newhelpers';
import { Router } from '@angular/router';
@Component({
    selector: 'demo-ordercard',
    templateUrl: './demo.component.html',
    providers: [Broadcaster, TranslationService],
})
export class DemoOrderCardComponent {
    constructor(private ev: Broadcaster, private t:TranslationService) {

   }
}