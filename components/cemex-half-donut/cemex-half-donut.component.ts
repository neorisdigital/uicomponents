import { Component,Input,OnChanges } from '@angular/core';
import {Logger} from '../../services/logger.service';
// webpack html imports
let template = require('./cemex-half-donut.component.html');

@Component({
  selector: 'cemex-half-donut',
  template: template,
  styleUrls: ['./cemex-half-donut.component.scss']
})
export class CemexHalfDonutComponent implements OnChanges {
  // Doughnut
  @Input() chartData: any[];
  @Input() firstNumber: number = 0;
  @Input() secondNumber: number = 0;
  @Input() thirdNumber: number = 0;

  public doughnutChartLabels:string[] = [];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';
  private doughnutChartColors: any[] = [{ backgroundColor: ["#b8436d", "#00d9f9", "#a4c73c", "#a4add3"] }];
  private chartOptions:any={rotation: 1 * Math.PI,circumference: 1 * Math.PI};

  constructor(){
    if(this.chartData==null || this.chartData==undefined){
      this.chartData=[];
    }
    if(this.chartData.length==0){
      this.chartData=[{label:"label num 1",value:300},{label:"label num 2",value:450}]
    }
    this.getValuesForChart();
    this.getLabelsForChart();
  }

  ngOnChanges(changes) {
      //console.log("----chart-changed");
      this.getLabelsForChart();
      this.getValuesForChart();
  }
  
  // events
  public chartClicked(e:any):void {
    // console.log(e);
  }

  public chartHovered(e:any):void {
    // console.log(e);
  }
  public getLabelsForChart():void{
    this.doughnutChartLabels=[];
    for(let item of this.chartData){
      this.doughnutChartLabels.push(item.label);
    }
  }
  public getValuesForChart():void{
    this.doughnutChartData=[];
    for(let item of this.chartData){
      this.doughnutChartData.push(item.value);
    }
  }
  private getLabelColor(index:number):string{
    //Logger.log(this.doughnutChartColors[0].backgroundColor[index]);
    //return this.doughnutChartColors[0].backgroundColor[index];
     if(index>3){
      this.doughnutChartColors[0].backgroundColor[index%4];
    }
    return this.doughnutChartColors[0].backgroundColor[index];
  }
}