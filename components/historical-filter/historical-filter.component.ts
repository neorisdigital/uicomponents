import { Component,EventEmitter, Output, OnInit, Input } from '@angular/core/'
import { Broadcaster} from '../../events/broadcaster.event';
import {TranslationService} from '../../services/translation.service';
import {Logger} from '../../services/logger.service';
let _  = require('underscore');
var moment = require('moment');

@Component({
  selector:     'historical-filter',
  templateUrl:  './historical-filter.component.html',
  providers: [TranslationService],
  styleUrls:   ['./historical-filter.component.scss']
})


export class HistoricalFilterComponent{
  @Input()dates:any;
  @Input()title:string;
  public isRange:boolean;
  public currentMonth:any = false;
  static CHANGE_MONTH_FILTER:string = "CHANGE_MONTH_FILTER";
  constructor(private eventDispatcher: Broadcaster,  private t:TranslationService){
       
  }

  ngOnChanges(){
    if(this.dates && this.dates.length){
       if(!this.currentMonth){
          this.currentMonth = this.dates[0].monthNumber;
          this.eventDispatcher.broadcast(HistoricalFilterComponent.CHANGE_MONTH_FILTER, this.dates[0]);
       } 
    }
  }

  public selectMonth(date):void{
    this.currentMonth = date.monthNumber;
    this.eventDispatcher.broadcast(HistoricalFilterComponent.CHANGE_MONTH_FILTER, date);
  }

}
