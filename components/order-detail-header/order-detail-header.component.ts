import { Component, Input } from '@angular/core';

// Providers needed
import { TranslationService } from '../../services/translation.service';
import { SidebarEvents } from '../../events/sidebar.event';

// Models, just a comment
import JobsiteDTO from '../../DTO/jobsiteDTO';
import CustomerDTO from '../../DTO/customerDTO';

@Component({
    selector: 'order-header',
    templateUrl: './order-detail-header.component.html',
    styleUrls: ['./order-detail-header.component.scss'],
    providers: [TranslationService]
})
export class OrderDetailHeaderComponent {
    @Input() order: any;
    @Input() customer: any;
    @Input() jobsite: any;
    constructor() {}
}
