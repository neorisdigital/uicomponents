import { Component,Input,OnInit } from '@angular/core';
import {nvD3} from 'ng2-nvd3'
// webpack html imports
let template = require('./nv3.component.html');
declare let d3: any;
@Component({
  selector: 'cemex-nv3',
  template: template,
  styleUrls: ['./nv3.component.scss']
})

export class Nv3Component  implements OnInit{
  options;
  data;
  ngOnInit(){
    this.options = {
      chart: {
      type: 'pieChart',
      height: 450,
      donut: true,
      x: function(d){return d.key;},
      y: function(d){return d.y;},
      showLabels: false,
      pie: {
        startAngle: function(d) { return d.startAngle/2 -Math.PI/2 },
        endAngle: function(d) { return d.endAngle/2 -Math.PI/2 }
      },
      duration: 500,
      legend: {
        position:'right'
        
      }
    }
    }
    this.data = [
      {
      key: "One",
      y: 5
    },
    {
      key: "Two",
      y: 2
    },
    {
      key: "Three",
      y: 9
    },
    {
      key: "Four",
      y: 7
    },
    {
      key: "Five",
      y: 4
    },
    {
      key: "Six",
      y: 3
    },
    {
      key: "Seven",
      y: .5
    }
    ];
    // d3.select(".nv-legendWrap").attr("transform","translate(0,350)");
  }
  
}