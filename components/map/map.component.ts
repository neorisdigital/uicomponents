import { Component, OnInit } from '@angular/core';
import { Ng2MapComponent }   from 'ng2-map';

// Components
import { SidebarComponent }           from '../../../uicomponents/components/sidebar/sidebar.component';
import { OrderTicketHeaderComponent } from '../../../uicomponents/components/order-ticket-header/order-ticket-header.component';
import { orderCardComponent }         from '../../../uicomponents/components/ordercard/ordercard.component';
import { JobsiteFilterComponent }     from '../../../uicomponents/components/jobsite-filter/jobsite-filter.component';

// DTOs
import OrderDTO     from '../../../uicomponents/DTO/orderDTO';
import TicketDTO     from '../../../uicomponents/DTO/ticketDTO';
import { UserDTO }  from '../../../uicomponents/DTO/userDTO';

// Events
import { Broadcaster } from '../../../uicomponents/events/broadcaster.event';

// Services
import { Logger }             from '../../../uicomponents/services/logger.service';
import { JobsiteService }     from '../../../uicomponents/services/jobsite.service';
import { OrderService }       from '../../../uicomponents/services/order.service';
import { SessionService }     from '../../../uicomponents/services/session.service';
import { TranslationService } from '../../../uicomponents/services/translation.service';

declare var google: any;
declare var marker: any;

@Component({
    selector: 'map',
    templateUrl: './map.html',
    styleUrls: ['./map.scss'],
    providers: [TranslationService,JobsiteService,OrderService,Broadcaster,SidebarComponent]
})

export class MapComponent implements OnInit {
	
    positions:any[];
  	count:number;
  	isTruck:string;

    order:OrderDTO;
    ticket:TicketDTO;

    private showOnRoute: boolean = true;
    private showOnSite: boolean = true;
    private showPouring:boolean = true;

    popup:boolean = false;

    public status: any = [];

  constructor(private t:TranslationService,
              private session:SessionService,
              private eventDispatcher:Broadcaster,
              private sideBarComponent:SidebarComponent,
              public jobsiteService:JobsiteService,
              public orderService:OrderService){
  		  
        this.count=1; 

        

       

        // this.eventDispatcher.on<OrderDTO>(Broadcaster.CHANGE_SCREEN_HOME)
        // .subscribe(response => {
        //   this.order = response;
        //   console.log('hey guy', response);
        // });

        // console.log('hey boi', orderCardComponent.lastSelectedOrder);

        this.orderService
          .getTicketbyId(orderCardComponent.lastSelectedOrder.orderId)
          .subscribe(result => {
            console.log('hey girl', result)
            this.ticket = result;
          });

        Ng2MapComponent['apiUrl'] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAXXlH9VEHMkc9zWwe45cNJj995HCdBXLI&callback=initMap';        
        this.showRandomMarkers();
	// this.showInfoWindow(marker)
    }

ngOnInit(){
	// this.getTix();
}
  showRandomMarkers() {
    let randomLat: number, randomLng: number;
    this.positions = [];
    for (let i = 0 ; i < 10; i++) {
      randomLat = Math.random() * 0.0799 + 25.7617;
      randomLng = Math.random() * 0.0799 + -80.2918;
      this.positions.push([randomLat, randomLng]);
    }
  }
  
 ngDoCheck(){
 	if(this.count > 2) {
 		this.isTruck ="A"
 		return true
 	}else{
 		this.isTruck =""
 	}
 } 

  showInfoWindow(marker) {
    marker.ng2MapComponent.openInfoWindow(
      'iw',    // id of InfoWindow
      marker,  // anchor for InfoWindow
      {        // local variables for InfoWindow
        lat: marker.getPosition().lat(),
        lng: marker.getPosition().lng(),
      }
    );
  } 




public clicked(t) {
    var e = t.target;
    e.Ng2MapComponent.openInfoWindow("iw", e, {
        lat: e.getPosition().lat(),
        lng: e.getPosition().lng()
    })
}

getTix(order:any){
  this.orderService
          .getTicketbyId(order.orderId)
          .subscribe(
            result => console.log(result)
          );
  this.status = [ { title: 'Active', color:'NST' }, { title: 'In Progress', color:'STR' }, { title: 'To be Confirmed', color:'WCL' }, { title: 'On Hold', color:'OHD' }, { title: 'Cancelled', color:'CNC' }, { title: 'Completed', color:'CMP' }, { title: 'Payment Hold', color:'PHD' } ];
}

private onTab(tab: any){
        if(tab == "all") {
            this.showOnRoute = true;
            this.showOnSite = true;
            this.showPouring = true;
        } else if(tab == "onroute") {
            this.showOnRoute = true;
            this.showOnSite = false;
            this.showPouring = false;
        } else if(tab == "onsite") {
            this.showOnRoute = false;
            this.showOnSite = true;
            this.showPouring = true;
        }
    }

  showPopup(item:any) {
    console.log('popup duh', item);
    this.popup = !this.popup;
    this.clicked(item)
  }

}
