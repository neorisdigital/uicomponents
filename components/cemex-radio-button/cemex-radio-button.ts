import { Component, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'cemex-radio-button',
  templateUrl: './cemex-radio-button.html',
  styleUrls: ['./cemex-radio-button.css'],
})
export class CemexRadioButtonComponent {
  @Input() title: string;

  constructor() {
    this.title = "radio";
  }


}