import { Component } from '@angular/core';
import { MdIcon,
    MdIconStyler,
    MdIconExtraLargeStyler,
    MdIconLargeStyler,
    MdIconMediumStyler,
    MdIconSmallStyler,
    MdIconExtraSmallStyler } from './index';

@Component({
    selector: 'demo-md-icon',
    templateUrl: './demo.component.html'
})
export class DemoMdIconComponent {
}