import { Directive } from '@angular/core';

@Directive({
    selector: 'span[md-icon]',
    host: {
        '[class.md-icon]': 'true'
    }
})
export class MdIconStyler{}

@Directive({
    selector: 'span[md-icon][extra-large-size]',
    host: {
        '[class.md-icon-extra-large]': 'true'
    }
})
export class MdIconExtraLargeStyler{}

@Directive({
    selector: 'span[md-icon][large-size]',
    host:{
        '[class.md-icon-large]': 'true'
    }
})
export class MdIconLargeStyler{}

@Directive({
    selector: 'span[md-icon][medium-size]',
    host:{
        '[class.md-icon-medium]': 'true'
    }
})
export class MdIconMediumStyler{}

@Directive({
    selector: 'span[md-icon][small-size]',
    host:{
        '[class.md-icon-small]': 'true'
    }
})
export class MdIconSmallStyler{}

@Directive({
    selector: 'span[md-icon][extra-small-size]',
    host:{
        '[class.md-icon-extra-small]': 'true'
    }
})
export class MdIconExtraSmallStyler{}

