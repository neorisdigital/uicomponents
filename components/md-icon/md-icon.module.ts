import { NgModule } from '@angular/core';
import { MdIcon } from './md-icon.component';
import {
    MdIconStyler,
    MdIconExtraLargeStyler,
    MdIconLargeStyler,
    MdIconMediumStyler,
    MdIconSmallStyler,
    MdIconExtraSmallStyler } from './md-icon.directive';

@NgModule({
    exports: [
        MdIcon,
        MdIconStyler,
        MdIconExtraLargeStyler,
        MdIconLargeStyler,
        MdIconMediumStyler,
        MdIconSmallStyler,
        MdIconExtraSmallStyler,
    ],
    declarations: [
        MdIcon,
        MdIconStyler,
        MdIconExtraLargeStyler,
        MdIconLargeStyler,
        MdIconMediumStyler,
        MdIconSmallStyler,
        MdIconExtraSmallStyler,
    ]
})
export class MdIconModule{
    static forRoot(){
        return {
            ngModule: MdIconModule,
            providers: []
        }
    }
}

