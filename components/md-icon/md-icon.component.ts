import { Component, Input, ViewEncapsulation, ElementRef, Renderer } from '@angular/core';

@Component({
    selector: 'span[md-icon]',
    templateUrl: './md-icon.component.html',
    styleUrls: [ './md-icon.component.scss' ],
    encapsulation: ViewEncapsulation.None,
})
export class MdIcon{
    private _elementRef: ElementRef;
    private _renderer: Renderer;
    private _icon: string;

    constructor( elementRef: ElementRef, renderer: Renderer ){
        this._elementRef = elementRef;
        this._renderer =  renderer;
    }
    
    @Input( 'md-set' )
    get icon(): string{
        return this._icon;
    }
    set icon( value: string ){
        this._updateIcon( value );
    }

    private _updateIcon( newIcon: string ): void{
        this._setElementColor( this._icon, false );
        this._setElementColor( newIcon, true );
        this._icon = newIcon;
    }
    
    private _setElementColor( icon: string, isAdd: boolean ){
        if( icon !== null && icon !== '' ){
            this._renderer.setElementClass( this._getHostElement(), icon, isAdd );
        }
    }

    private _getHostElement(): any{
        return this._elementRef.nativeElement;
    }
}