import { Component } from '@angular/core';
import {TranslationService} from '../../services/translation.service';

@Component({
    selector: 'simple-card-example',
    templateUrl: './demo.component.html',
    providers: [TranslationService]
})

export class DemoCemexSimpleCardComponent{
   constructor(private t:TranslationService) {}
}