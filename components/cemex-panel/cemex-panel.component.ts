import { Component, Injectable, NgZone, EventEmitter, Output, Input, OnInit } from '@angular/core';


export enum PanelType {
    primary = 1,
    secondary = 2,
    tirth = 3,
    // disabled = 3
}

// export enum PanelSize {
//     normal = 1,
//     medium = 2,
//     small = 3
// }

export enum FontSize {
    fontnormal = 1,
    fontmedium = 2,
    fontsmall = 3
}



@Component({
    selector: 'cemex-panel',
    templateUrl: './cemex-panel.component.html',
    styleUrls: ['./cemex-panel.component.css']


})


export class CemexPanelComponent implements OnInit {


    @Input() titlehead: string;
    @Input() titledescription: string;

    @Input() fonthead: string;
    @Input() fontdescription: string;

    @Input() type: PanelType;
    // @Input() size: PanelSize;
    @Input() fontsizehead: FontSize;
    @Input() fontsizedescription: FontSize;





    constructor() {


        this.titlehead = "head";
        this.fontdescription = "description";
        this.type = PanelType.primary;
        // this.size = PanelSize.normal;
        this.fontsizehead = FontSize.fontnormal;
        this.fontsizedescription = FontSize.fontmedium;

    }


    ngOnInit(): void {
        // this.authService.logout();

    }
    getStyle() {
        return this.getTypeClass();//+ " " + this.getSizeClass()
    }

    getTypeClass() {
        if (this.type == PanelType.primary) {
            return "primary"
        }
        else if (this.type == PanelType.secondary) {
            return "secondary"
        }
        else if (this.type == PanelType.tirth) {
            return "tirth"
        }
        else {
            // defualt case
            return ""
        }
    }

    // getSizeClass() {
    //     if (this.size == PanelSize.normal) {
    //         return "normal"
    //     }
    //     else if (this.size == PanelSize.medium) {
    //         return "medium"
    //     }
    //     else if (this.size == PanelSize.small) {
    //         return "small"
    //     }
    //     else {
    //         // defualt case
    //         return ""
    //     }
    // }


    getSizeFontHeadClass() {

        if (this.fontsizehead == FontSize.fontnormal) {
            return "fontnormal"
        }
        else if (this.fontsizehead == FontSize.fontmedium) {
            return "fontmedium"
        }
        else if (this.fontsizehead == FontSize.fontsmall) {
            return "fontsmall"
        }
        else {
            // defualt case
            return ""
        }
    }


    getSizeFontDescriptionClass() {

        if (this.fontsizedescription == FontSize.fontnormal) {
            return "fontnormal"
        }
        else if (this.fontsizedescription == FontSize.fontmedium) {
            return "fontmedium"
        }
        else if (this.fontsizedescription == FontSize.fontsmall) {
            return "fontsmall"
        }
        else {
            // defualt case
            return ""
        }
    }



}