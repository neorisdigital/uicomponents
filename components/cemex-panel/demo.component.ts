import { Component } from '@angular/core';
import { CemexPanelComponent, FontSize, PanelType } from '../cemex-panel/cemex-panel.component';
@Component({
    selector: 'demo-cemex-panel',
    templateUrl: './demo.component.html'
})
export class DemoCemexPanelComponent {


  headvar1: string = "$21,345.65";
  descriptionvar1: string = "1 to 7 days";
  headvar2: string = "$28,445.65";
  descriptionvar2: string = "8 to 30 days";
  headvar3: string = "$32,344.43";
  descriptionvar3: string = "31 + days";
  headvar4: string = "$2,354.43";
  descriptionvar4: string = "1 to 3 days";
  headvar5: string = "$8,543.78";
  descriptionvar5: string = "4 to 14 days";
  headvar6: string = "$12,432.98";
  descriptionvar6: string = "15 + days";



  panelPrimary: PanelType = PanelType.primary;
  panelSecondary: PanelType = PanelType.secondary;
  paneltirth: PanelType = PanelType.tirth;


  // panelNormal: PanelSize = PanelSize.normal;
  // panelMedium: PanelSize = PanelSize.medium;
  // panelSmall: PanelSize = PanelSize.small;

  fontSizeNormal: FontSize = FontSize.fontnormal;
  fontSizeMedium: FontSize = FontSize.fontmedium;
  fontSizeSmall: FontSize = FontSize.fontsmall;

}

