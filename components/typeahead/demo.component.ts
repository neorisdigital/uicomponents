import { Component } from '@angular/core';
@Component({
    selector: 'demo-typeahead',
    templateUrl: './demo.component.html'
})
export class DemoTypeaheadComponent {
   public fruits:Array<any>  = [{ name: 'mango'}, {name: 'piña'}, {name: 'platano'}, {name: 'manzana'}];
   public key:string = "name";
   public placeholder:string = "search a fruit";
}