import { Component, Input, OnChanges } from '@angular/core';
import { Logger } from '../../services/logger.service';

@Component({
    selector: 'typeahead',
    templateUrl: './typeahead.component.html',
    styleUrls: ['./typeahead.component.scss']
})
export class TypeaheadComponent{
    @Input() placeholder: string;
    @Input() items: Array<any>;
    @Input() searchBy: string;
    public searchInput: string;
    public foundItems = [];
    public search = function () {
        this.foundItems = this.findItems();
    }

    public navigateItems = function(e){
        if(e.keyCode == 40 || e.keyCode == 38){
            console.log("here", e.keyCode);
            return false;
        }
    }
    public findItems = function () {
        if (!this.searchInput || this.searchInput == "") { return []; }
        let newItems = [];
        for (let item of this.items){
            let x = new RegExp(this.searchInput, "g");
            item.name.match(x) ? newItems.push(item) : false;
        }
        return newItems;
    }
    constructor() {
    
    }
}
