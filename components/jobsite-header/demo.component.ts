import { Component } from '@angular/core'
import { JobsiteHeaderComponent } from './jobsite-header.component'
import { TranslationService } from '../../services/translation.service';

@Component({
    selector:     'demo-jh',
    templateUrl:  './demo.component.html',
    providers: [TranslationService]
  })
export class DemoJobsiteHeaderComponent {

}
