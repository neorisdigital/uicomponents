import { Component, EventEmitter, Input, Output }  from '@angular/core'
import { TranslationService } from '../../services/translation.service';
import { Logger } from '../../services/logger.service';
import { Broadcaster } from '../../../uicomponents/events/broadcaster.event';
import { orderCardComponent } from '../../components/ordercard/ordercard.component';
//import { DashboardComponent } from '../../../app/components/dashboard/dashboard.component'

import JobsiteDTO from '../../DTO/jobsiteDTO';
import CustomerDTO from '../../DTO/customerDTO';
// import OrderDTO from '../../DTO/OrderDTO';

import { SidebarEvents } from '../../events/sidebar.event';
import { SidebarComponent } from '../../components/sidebar/sidebar.component';
import { CalendarEvents } from '../../../uicomponents/events/calendar.event'

import { CoreService } from '../../../uicomponents/services/core/core.service';
import { Subscription } from 'rxjs/Subscription';
import { JobsiteService } from '../../../uicomponents/services/jobsite.service';

@Component({
  selector:     'jobsite-header',
  templateUrl:  './jobsite-header.component.html',
  providers:    [ TranslationService, JobsiteService, CoreService ],
  styleUrls:    ['./jobsite-header.component.scss']
})

export class JobsiteHeaderComponent {
  @Input() customer: CustomerDTO;
  @Input() jobsite: JobsiteDTO;

  public static GO_HOME:string="go_home"
  public static GO_SUB_MENU1:string="go_sub_menu1"
  public static GO_SUB_MENU2:string="go_sub_menu2"
  
  public static jobsite: JobsiteDTO;
  public static customer:CustomerDTO;
  public static num_orders: number = 0;
  public currentOrder: any;

  public ordersQty: number = 0;
  public isFave: boolean;
  public fave: string = "off";
  public back: string = "";

  subscription: Subscription;


  constructor(private t:TranslationService, private eventDispatcher:Broadcaster,
              private sidebarListener: SidebarEvents,private calendar: CalendarEvents,
              private core: CoreService, private JobsiteService: JobsiteService) {
                this.currentOrder=orderCardComponent.lastSelectedOrder;
                this.turnText();
                console.log(this.currentOrder);
                if(JobsiteHeaderComponent.customer){
                  this.customer=JobsiteHeaderComponent.customer;
                  this.jobsite=JobsiteHeaderComponent.jobsite;
                  //this.ordersQty=JobsiteHeaderComponent.jobsite.orders.length;
                }

      this.eventDispatcher.on<JobsiteDTO>(SidebarComponent.JOBSITE_SELECTED_DTO)
        .subscribe(response => {
          this.jobsite=response;
          this.checkFavorite();
      });

     this.eventDispatcher.on<CustomerDTO>(SidebarComponent.CUSTOMER_SELECTED)
      .subscribe(response => {
      this.customer = response;
    });

    this.sidebarListener.getJobsiteSelected().subscribe(jobsite => {
            if (!jobsite) { return; }
            JobsiteHeaderComponent.jobsite = jobsite;
            JobsiteHeaderComponent.num_orders=jobsite.orders.length;
            this.jobsite=jobsite;
            this.ordersQty=jobsite.orders.length;
        });

    this.sidebarListener.getCustomerSelected().subscribe(customer=>{
      if(customer){
        JobsiteHeaderComponent.customer=customer;
        this.customer=customer;
      }
      
      
    });
                 
  }

  OnInit() {
    this.checkFavorite();
    this.turnText();
  }

  addFavorite(jobsite:JobsiteDTO):any {
    this.isFave = !this.isFave;
    this.checkFavorite();
    this.changeFavorite(jobsite);
  }

  checkFavorite() {
    if (this.jobsite.favoriteStatus == 'Y'){
      this.fave = "on";
      return this.fave;
    } else {
      this.fave = "off";
      return this.fave;;
    }
  }

  changeFavorite(statusChange:JobsiteDTO) {
    let jobArray: any = [];
    if (this.jobsite.favoriteStatus == 'Y'){
      this.fave = "off";
      statusChange.favoriteStatus = 'N';
      jobArray = {  jobsites: [
                      {
                        jobsiteId: statusChange.jobsiteId,
                        favoriteStatus: "N"
                      }
                    ]
                  };
      this.JobsiteService
          .changeFavoriteStatus(jobArray)
          .subscribe(
                      result => console.log(result)
                    );
      return this.fave;
    } else {
      this.fave = "on";
      statusChange.favoriteStatus = 'Y';
      jobArray = {  jobsites: [
                      {
                        jobsiteId: statusChange.jobsiteId,
                        favoriteStatus: "Y"
                      }
                    ]
                  };
      this.JobsiteService
          .changeFavoriteStatus(jobArray)
          .subscribe(
                      result => console.log(result)
                    );
      return this.fave;
    }
  }

  searchFocused() {
    //DashboardComponent.openSearch();
  }

  public getJobsite(){
    return JobsiteHeaderComponent.jobsite;
  }
  public gotoClient(){
    orderCardComponent.lastSelectedOrder=null;
    this.eventDispatcher.broadcast(JobsiteHeaderComponent.GO_HOME);
  }
  
  public gotoJobsite(){
    orderCardComponent.lastSelectedOrder=null;
    this.eventDispatcher.broadcast(JobsiteHeaderComponent.GO_SUB_MENU1);
  }

  shouldShowOrder():boolean{
      return orderCardComponent.lastSelectedOrder!=null;
  }

  turnText() {
    if (this.shouldShowOrder()){
      this.back = "back";
    } else {
      this.back = "";
    }
  }

}

