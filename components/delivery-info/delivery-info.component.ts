import { Component, Input,OnInit } from '@angular/core';
import { TranslationService } from '../../services/translation.service';

import OrderDTO from '../../DTO/orderDTO';

@Component({
    selector: 'delivery-info',
    templateUrl: './delivery-info.component.html',
    styleUrls: ['./delivery-info.component.scss'],
    providers: [TranslationService]
})
export class DeliveryInfoComponent implements OnInit{
    @Input() order: any;

    public orderProducts:Map<string,any>=new Map<string,any>();
    public orderProductsPlan=[];
    constructor(private t:TranslationService) {
        
    }

    ngOnInit() {
        for(let item of this.order.orderItem){
            if(!this.orderProducts.has(item.productCode)){
                this.orderProducts.set(item.productCode,{desc:item.productDesc,quantity:item.productQuantity,unit:item.unitDesc,element:item.element,loads:1})
            } else {
                let props=this.orderProducts.get(item.productCode);
                props.quantity=props.quantity+item.productQuantity
                props.loads=props.loads+1;
                this.orderProducts.set(item.productCode,props);
            }
        }
        this.orderProductsPlan=Array.from(this.orderProducts.values());
        //console.log(this.orderProductsPlan);
    }
    public getProductsAsArray(){
        //let innnerArray=[];

    }

    isReadyMix(): boolean{
        //console.log('in isreadymix');
        if(this.order.isReadyMix == true){
            return true;
        } else {
            return false;
        }
        // return true
    }

    isCement(): boolean{
        //console.log('in cement');
        if(this.order.isReadyMix == true){
            return false;
        } else {
            return true;
        }
        // return false;
    }
}