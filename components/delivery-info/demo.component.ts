import { Component } from '@angular/core';
import {SessionService} from '../../services/session.service';
import {HttpCemex} from '../../services/http.service';
import {CredentialsConstants} from '../../constants/credentials.constants';
import {Broadcaster} from '../../events/broadcaster.event';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';


import OrderDTO from '../../DTO/orderDTO';


@Component({
    selector: 'delivery-info-example',
    templateUrl: './demo.component.html',
    providers: [SessionService,HttpCemex,Broadcaster,TranslationService]
})

export class DemoDeliveryInfoComponent{
constructor(private loginService:SessionService,private eventDispatcher:Broadcaster,private t:TranslationService){

  
}


public shouldShowOrder():OrderDTO{
        var myOrder:OrderDTO  = new OrderDTO();

        myOrder.deliveredQuantity = 10;
        myOrder.instructionsDesc = "instructionsDesc";
        myOrder.isReadyMix = true;
        myOrder.orderCode = "5000";
        myOrder.orderId = 1;
        myOrder.orderStatus = "orderStatus";
        myOrder.orderStatusDesc = "orderStatusDesc";
        myOrder.orderStatusGroupCode = "1";
        myOrder.programedDateTime = new Date();
        myOrder.purchaseOrder = "1200";
        myOrder.shippingConditionId = 1;
        myOrder.totalDeliveries = 10;


        return myOrder;
    }

}