import { Pipe, PipeTransform } from "@angular/core";
 
@Pipe({
    name: "orderBy",
    pure: false
})
export class OrderByPipe implements PipeTransform {
 
    transform(items: Array<any>, key: string, asc: boolean) {
        items.sort((n1,n2) => {
            if (n1[key] > n2[key]) {
                return asc ? 1 : -1;
            }
            if (n1[key] < n2[key]) {
                return asc? -1: 1;
            }
            return 0;
        });
        return items;
    }
}