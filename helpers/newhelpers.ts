
export default class GeneralHelper {
  static findBy = function(field, value) {
        for (var i = 0; i < this.length; i++) {
            if (!this[i][field] || this[i][field] != value) {
                continue;
            }
            return this[i];
        }
    }
    static findAllBy = function(field, value) {
        var result = [];

        for (var i = 0; i < this.length; i++) {
            if (!this[i][field] || this[i][field] != value) {
                continue;
            }
            result.push(this[i]);
        }

        return result;
    }
    static removeBy = function (field, value) {
        for (var i = 0; i < this.length; i++) {
            if (!this[i][field] || this[i][field] != value) {
                continue;
            }
            this.splice(i, 1);
            return;
        }
    }
    static removeById = function (id) {
        return this.removeBy("id", id);
    }
}
