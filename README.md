# README #



### What is this repository for? ###

* Store a generic, useful and shareable component implementation definition

### How do I get set up? ###

* This project IS NOT runnable, it has to be included from other projects as a GIT SUBMODULE


### Contribution guidelines ###

* This project does not use branches because is not runnable and if someone is working in a new component and it is not used in any project, the impact of breaking someone else's project is minimal
* Make sure you store DTO and http services in its right folders

### Who do I talk to? ###

* Repo owner or admin
* or directly to Andres Santos