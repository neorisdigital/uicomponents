import { Injectable }      from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Api
import { ApiService } from '../services/http.api.service';

// Models
import JobsiteDTO from '../DTO/jobsiteDTO';
import CustomerDTO from '../DTO/customerDTO';

var moment = require('moment');

@Injectable()
export class CalendarEvents {
    // Observable items sources
    private datesSubject = new BehaviorSubject<any>({
        start: moment(),
        end: moment()
    });

    // Observable items streams
    private dates$ = this.datesSubject.asObservable();

    setDates(dates: any) {
        this.datesSubject.next(dates);
    }

    getDates() {
        return this.dates$;
    }
}