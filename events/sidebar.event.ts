import { Injectable }      from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Api
import { ApiService } from '../services/http.api.service';

// Models
import JobsiteDTO from '../DTO/jobsiteDTO';
import CustomerDTO from '../DTO/customerDTO';

@Injectable()
export class SidebarEvents {
    // Observable items sources
    private jobsiteSubject = new BehaviorSubject<JobsiteDTO>(null);
    private customerSubject = new BehaviorSubject<CustomerDTO>(null);

    // Observable items streams
    private jobsite$ = this.jobsiteSubject.asObservable();
    private customer$ = this.customerSubject.asObservable();

    selectJobsite(jobsite: JobsiteDTO) {
        this.jobsiteSubject.next(jobsite);
    }

    selectCustomer(customer: CustomerDTO) {
        this.customerSubject.next(customer);
    }

    getJobsiteSelected() {
        return this.jobsite$;
    }

    getCustomerSelected() {
        return this.customer$;
    }
}