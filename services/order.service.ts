import { Injectable } from '@angular/core';
import { HttpModule, RequestOptions,Headers,Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpCemex}    from '../../uicomponents/services/http.service'

import 'rxjs/add/operator/map';
import { PathConstants} from '../constants/path.constants';

@Injectable()
export class OrderService {
    public static startdate:string="2016-12-01";
    public static enddate:string="2016-12-31";

    constructor(private http: HttpCemex) {}

    public getOrders(jobsiteId: number) {
        return this.http
            .get(PathConstants.getWorkingPath()+ PathConstants.JOBSITES_PATH+ jobsiteId+'/orders?startdate='+OrderService.startdate+'%2000:00&enddate='+OrderService.enddate+"%2023:59")
            .map(res => res.json())
    }

    public getOrdersByDate(jobsiteId: number,startdate:string,enddate:string) {
        return this.http
            .get(PathConstants.getWorkingPath()+ PathConstants.JOBSITES_PATH+ jobsiteId+'/orders?startdate='+startdate+'%2000:00&enddate='+enddate+"%2023:59")
            .map(res => res.json())
    }

    public getTicketbyId(orderId: number) {
        return this.http
            .get(PathConstants.getWorkingPath()+ PathConstants.TICKET_PATH + orderId)
            .map(res => res.json())
    }

    public getMarker(orderId: number, ticketId:number) {
        return this.http
            .get(PathConstants.getWorkingPath()+ PathConstants.MARKER_PATH + orderId + '/locations?ticketId='+ ticketId)
            .map(res => res.json())
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
