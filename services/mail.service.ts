import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Mail } from './../DTO/mailDTO';

@Injectable()
export class MailService{
    private _http: Http;
    private _baseUrl: string;
    private _path: string;
    private _api: string;

    constructor( http: Http ){
        this._http = http;
        //TODO: MOVE TO PATH CONSTANTS
        this._baseUrl = "https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-development/";
        this._path = "dev/v1/rc/";
        this._api = "emailsimple";
    }

    public sendMail( mail: Mail ): Promise<any>{
        return this._http.post( this._getUrl(), mail ).toPromise()
        .then(
            response => {
                return response.json();
            }
        )
        .catch(
            error => error.json()
        )
    }

    private _getUrl(): string{
        return this._baseUrl + this._path + this._api;
    }
}