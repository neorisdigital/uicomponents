/**
 * Usage example:
 * constructor(api: ApiService) {
        api.get('jobsites').subscribe(result => {
            console.log(result.json());
        });
    }
 */

import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CredentialsConstants } from '../constants/credentials.constants';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService extends Http {
    private base: string;

    constructor (backend: XHRBackend, options: RequestOptions, base?: string) {
        super(backend, options);
        this.base = 'https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-quality/'
        options.headers = this.makeHeader();
    }
    
    request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
        options = this.makeHeader()
        return super.request(url, options).catch(this.catchAuthError(this));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.base + url;
        return super.get(url, options);
    }

    private makeHeader(): Headers {
        let options = new RequestOptions();
        if (options == null || options == undefined) { options=new RequestOptions(); }
        if (options.headers == null || options.headers == undefined) { options.headers=new Headers(); }
        let headers = options.headers
        
        if (CredentialsConstants.getBearer() != null 
            && CredentialsConstants.getBearer() != undefined 
                && CredentialsConstants.getBearer() != "") { headers.append('Authorization', 'Bearer ' + CredentialsConstants.getBearer()); }
        
        if (CredentialsConstants.getAppCode() != "") {
            headers.append('App-Code',CredentialsConstants.getAppCode());
            headers.append('AppCode',CredentialsConstants.getAppCode());
        } 
        else { console.error("No app-code was set in the CredentialConstants"); }
        headers.append('x-ibm-client-id',CredentialsConstants.getKey());
        
        if (CredentialsConstants.getJWT() != "") { headers.append('jwt',CredentialsConstants.getJWT()); } 
        else { console.error("No JWT was set in the CredentialConstants"); }
        
        return headers;
    }
        
    private catchAuthError (self: ApiService) {
        // We have to pass ApiService's own instance here as `self`
        return (res: Response) => {
            console.log(res);
            if (res.status === 401 || res.status === 403) {
                // If not authenticated
                console.log(res);
            }
            return Observable.throw(res);
        };
    }
}