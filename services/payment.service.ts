﻿import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

@Injectable()
export class PaymentService{
    private _http: Http;
    private _baseUrl: string;
    private _path: string;
    protected api: string;

    constructor( http: Http ){
        this._http = http;
        //TODO: MOVE TO PATH CONSTANTS
        this._baseUrl = "https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-development/";
        this._path = "dev/v1/arm/";
        this.api = "referencepayment";
    }

    //payment reference to api connect previous opening bank page
    public sendReference(){}

    private _getUrl(): string {
        return this._baseUrl + this._path + this.api;
    }
}