﻿import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { LegalEntity } from './../DTO/legalEntityDTO';

@Injectable()
export class LegalEntityService  {
    private _http: Http;
    private _baseUrl: string;
    private _path: string;
    private _api: string;

    constructor( http: Http) {
        this._http = http;
        //TODO: MOVE TO PATH CONSTANTS
        this._baseUrl = "https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-development/";
        this._path = "dev/v1/arm/";
        this._api = "legalentity";
    }

    public getLegalEntity(): Promise<LegalEntity>{
        return this._http.get( this._getUrl() ).toPromise()
        .then(
            legalEntity => {
                return legalEntity.json();
            }
        )
        .catch(
            error => error.json()
        );
    }
    
    public getLegalEntityCustomersByAccount( idAccount: string): Promise<LegalEntity>{
        var url = this._baseUrl + this._path + 'legalentitycustomers?idAccount=' + idAccount;
        return this._http.get( url ).toPromise()
        .then(
            customers => {
                return customers.json();
            }
        )
        .catch(
            error => error.json()
        );
    }

    public getLegalEntityByJobsites( idAccount: string, idCustomer: string ): Promise<LegalEntity>{
        var url = this._baseUrl + this._path + 'legalentityjobsites?idAccount=' + idAccount + '&idCustomer=' + idCustomer;
        return this._http.get( url ).toPromise()
        .then(
            jobsites => {
                return jobsites.json();
            }
        )
        .catch(
            error => error.json()
        );
    }

    private _getUrl(): string{
        return this._baseUrl + this._path + this._api;
    }
}