import { Injectable } from '@angular/core';
import {HttpModule, RequestOptions, Headers} from '@angular/http';
import { UserDTO } from '../DTO/userDTO';
import {PathConstants} from '../constants/path.constants';
import 'rxjs/add/operator/toPromise';
import {CredentialsConstants} from '../constants/credentials.constants';
import {HttpCemex} from './http.service';
import {LoginDTO} from '../DTO/loginDTO';
import {Logger} from '../services/logger.service';
import {Broadcaster} from '../events/broadcaster.event';


@Injectable()
export class SessionService {
    constructor(private http: HttpCemex, private eventDispatcher: Broadcaster) {
        this.validateToken();
    }

    static currentSession: UserDTO=new UserDTO();

    private validateToken() {
        if (this.tokenExists()) {
            CredentialsConstants.setJWT(sessionStorage.getItem('jwt'));
            CredentialsConstants.setBearer(sessionStorage.getItem('auth_token'));
            this.getUserInfo();
        }
    }

    private tokenExists(): boolean {
        if (typeof sessionStorage !== 'undefined') {
            return this.checkExpiredSession(new Date(parseInt(sessionStorage.getItem('session_date'))), new Date()) &&
                !! sessionStorage.getItem('auth_token') &&
                !! sessionStorage.getItem('jwt') &&
                !! sessionStorage.getItem('session_date');
        }

        return false;
    }

    private checkExpiredSession(startDate: Date, currentDate: Date): boolean {
        var diff = (currentDate.getTime() - startDate.getTime()) / 60000;
        if (diff < 60)
            return true;
        else {
            this.clean();
            return false;
        }
    }

    isLoggedIn() {
        return this.tokenExists();
    }

    static LOGIN_SUCCESS_EVENT="LOGIN_SUCCESS_EVENT";
    static LOGIN_FAIL_EVENT="LOGIN_FAIL_EVENT";
    static LOGIN_LOGOUT_EVENT ="LOGIN_LOGOUT_EVENT";

    login(user: string, password: string): Promise<any> {
        this.clean();

        let options = new RequestOptions({
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' })
        });

        return this.http.post(PathConstants.getWorkingPath() + PathConstants.LOGIN_PATH,
                this.generateUrlString(user, password),
                options)
            .toPromise()
            .then(res => this.processIncomingDataFromLogin(res.json() as LoginDTO))
            .catch((ex) => {
                this.eventDispatcher.broadcast(SessionService.LOGIN_FAIL_EVENT);
                console.error('An error occurred', ex);
                return Promise.reject(ex.message || ex);
            });
    }

    getSession(): UserDTO {
        return SessionService.currentSession;
    }

    private processIncomingDataFromLogin(data: LoginDTO) {
        CredentialsConstants.setJWT(data.jwt);
        CredentialsConstants.setBearer(data.oauth2.access_token);
        sessionStorage.setItem('auth_token', data.oauth2.access_token);
        sessionStorage.setItem('jwt', data.jwt);
        sessionStorage.setItem('session_date', JSON.stringify(+new Date));
        this.getUserInfo();
    }

    private getUserInfo(): void {
        Logger.log("Getting user info..");
        this.http.get(PathConstants.getWorkingPath() + PathConstants.USER_INFO)
            .toPromise()
            .then(response => this.finalStepInSessionProcess(response.json() as UserDTO))
            .catch((ex) => {
                this.eventDispatcher.broadcast(SessionService.LOGIN_FAIL_EVENT);
                console.error('An error occurred', ex);
                return Promise.reject(ex.message || ex);
            });
    }

    private finalStepInSessionProcess(user: UserDTO): void {
        SessionService.currentSession = user;
        this.eventDispatcher.broadcast(SessionService.LOGIN_SUCCESS_EVENT);
    }

    logout(): void {
        this.clean();
        this.eventDispatcher.broadcast(SessionService.LOGIN_LOGOUT_EVENT);
    }

    private generateUrlString(user: string, password: string): string {
        return "grant_type=password&scope=security&username=" +
            user +
            "&password=" +
            password +
            "&client_id=" +
            CredentialsConstants.getKey();
    }

    private clean(): void {
        CredentialsConstants.setJWT(undefined);
        CredentialsConstants.setBearer(undefined);
        if (typeof sessionStorage !== 'undefined') {
            sessionStorage.removeItem('auth_token');
            sessionStorage.removeItem('jwt');
            sessionStorage.removeItem('session_date');
        }
        SessionService.currentSession = null;
    }


}
