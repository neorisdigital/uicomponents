﻿import { Injectable, Inject } from "@angular/core";
import { HttpModule, RequestOptions, Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import DocumentsDTO from '../DTO/documentsDTO';

@Injectable()
export class InvoiceService {
    private baseUrl: any;
    private path: any;
    protected api: any;

    constructor(private http: Http) {
        //TODO: MOVE TO PATH CONSTANTS
        this.baseUrl = "https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-development/";
        this.path = "dev/v1/arm/";
        this.api = "financialdocuments";
    }

    getAllInvoices(): Promise<DocumentsDTO[]> {

        return this.http.get(this.getUrl() + "?status=A")
            .toPromise()
            .then(response => response.json().documents)
            .catch(this.handleError);
    }

    getClosedInvoices() {
        return this.http.get(this.getUrl() + "?status=C")
            .map(res => res.json()).toPromise();
    }

    getOpenInvoices() {
        return this.http.get(this.getUrl() + "?status=O")
            .map(res => res.json()).toPromise();
    }

    private getUrl(): string {
        return this.baseUrl + this.path + this.api;
    }

    private handleError(error: any) {
        let errMsg = (error.message)
            ? error.message
            : error.status
            ? `${error.status} - ${error.statusText}`
            : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}