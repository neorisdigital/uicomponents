import { Injectable } from '@angular/core';
import {HttpModule, RequestOptions, Headers, Http, Response} from '@angular/http';
import {PathConstants} from '../constants/path.constants';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import JobsiteDTO from '../DTO/jobsiteDTO';

// Observable class extensions
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Injectable()

//comments
export class JobsiteService {

    constructor(private http: Http) { }

    public static jobsiteCache:any;

    getAllJobsites():Promise<JobsiteDTO[]> {
        return this.http.get(PathConstants.getWorkingPath()+PathConstants.PLANT_PATH)
               .toPromise()
               .then(response => response.json().data )
               .catch(this.handleError);
    }

    public changeFavoriteStatus(params:any) {
        let body = JSON.stringify(params);
        return this.http
            .patch(PathConstants.getWorkingPath()+PathConstants.JOBSITES_FAVORITE_PATH, params)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
       let body = res.json();
       return body || {};
   }

    // private handleError(error: any): Promise<any> {
    //     console.error('An error occurred', error);
    //     return Promise.reject(error.message || error);
    // }

    private handleError(error: any) {
       let errMsg = (error.message) ? error.message :
           error.status ? `${error.status} - ${error.statusText}` : 'Server error';
       console.error(errMsg);
       return Observable.throw(errMsg);
   }
}