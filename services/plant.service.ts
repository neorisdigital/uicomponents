import { Injectable } from '@angular/core';
import {HttpModule, RequestOptions,Headers,Http} from '@angular/http';
import { PlantDTO } from '../DTO/plantDTO';
import {PathConstants} from '../constants/path.constants';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PlantService {
    constructor(private http: Http) { }
    getAllPlants():Promise<PlantDTO[]> {
        return this.http.get(PathConstants.getWorkingPath()+PathConstants.PLANT_PATH)
               .toPromise()
               .then(response => response.json().data as PlantDTO[])
               .catch(this.handleError);
    }

    updateMonitoredPlants(arrayPlantIds):Promise<any> {
        return this.http.patch(PathConstants.getWorkingPath()+PathConstants.PLANT_PATH,JSON.stringify(arrayPlantIds))
                .toPromise()
                .then(response=>response.json().data)
                .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getPlantById():PlantDTO{

        return new PlantDTO();
    }
}