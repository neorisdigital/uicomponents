import {Injectable} from '@angular/core';
import {Http, Headers,RequestOptionsArgs,Request,RequestOptions} from '@angular/http';
import {CredentialsConstants} from '../constants/credentials.constants';

@Injectable()
export class HttpCemex {

  constructor(private http: Http) {}

  createAuthorizationHeader(options: RequestOptionsArgs):Headers {
    if(options == null || options == undefined) {
      options=new RequestOptions();
    }
    
    if(options.headers == null || options.headers == undefined) {
        options.headers=new Headers();
    }
    let headers=options.headers
    
    if(CredentialsConstants.getBearer() != null && CredentialsConstants.getBearer() != undefined && CredentialsConstants.getBearer()!="") {
      headers.append('Authorization', 'Bearer ' +CredentialsConstants.getBearer());
    }
    
    if (CredentialsConstants.getAppCode() != "") {
        headers.append('App-Code',CredentialsConstants.getAppCode());
        headers.append('AppCode',CredentialsConstants.getAppCode());
    } else {
        console.error("No app-code was set in the CredentialConstants");
    }
    
    headers.append('x-ibm-client-id',CredentialsConstants.getKey());
    
    if(CredentialsConstants.getJWT() != "") {
        headers.append('jwt',CredentialsConstants.getJWT());
    } else {
        console.error("No JWT was set in the CredentialConstants");
    }
    return headers;
    
  }

  public request(url: string | Request, options?: RequestOptionsArgs) {
      let loptions:RequestOptionsArgs = new RequestOptions();
    if(options!=undefined && options != null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.request(url, loptions);
  }

  public get(url: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.get(url, loptions);
  }

  public post(url: string, body: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.post(url, body, loptions);
  }

  public put(url: string, body: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.put(url, body, loptions);
  }

  public delete(url: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.delete(url, loptions);
  }

  public patch(url: string, body: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.patch(url, body, loptions);
  }

  public head(url: string, options?: RequestOptionsArgs) {
    let loptions:RequestOptionsArgs=new RequestOptions();
    if(options!=undefined && options!=null) {
      loptions=options;
    }
    this.createAuthorizationHeader(loptions);
    return this.http.head(url, loptions);
  }


}