﻿import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Bank } from './../DTO/bankDTO';

@Injectable()
export class BankService {
    private _http: Http;
    private _baseUrl: string;
    private _path: string;
    protected api: string;

    constructor( http: Http ){
        this._http = http;
        //TODO: MOVE TO PATH CONSTANTS
        this._baseUrl = "https://api.us.apiconnect.ibmcloud.com/cnx-gbl-org-development/";
        this._path = "dev/v1/arm/";
        this.api = "paymentsbanklist";
    }

    public getBankList(): Promise<Bank>{
        return this._http.get( this._getUrl() ).toPromise()
            .then(
                bank => {
                    return bank.json();
                }
            ).catch(
                error => error.json()
            );
    }

    private _getUrl(): string {
        return this._baseUrl + this._path + this.api;
    }
}