import { Injectable }      from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

// Listeners
import { CalendarEvents } from '../../events/calendar.event'

// Api
import { ApiService } from '../../services/http.api.service';

// Models
import JobsiteDTO from '../../DTO/jobsiteDTO';
import CustomerDTO from '../../DTO/customerDTO';

@Injectable()
export class CoreService {
    // Observable items sources
    private jobsitesSubject = new BehaviorSubject<any[]>([]);
    private customersSubject = new BehaviorSubject<any[]>([]);
    private loadingSubject = new Subject<boolean>();

    // Observable items streams
    jobsites$ = this.jobsitesSubject.asObservable();
    customers$ = this.customersSubject.asObservable();
    loading$ = this.loadingSubject.asObservable();

    constructor(private api: ApiService, private calendar: CalendarEvents) {
        //var date = new Date();
        //let today = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
        
        // Core calls
        //this.fetchJobsites(today, today);
        this.fetchCustomers();
        this.calendar.getDates().subscribe(dates => {
            if (!dates) { return; }
            this.loadingSubject.next(true);
            this.fetchJobsites(dates.start.format("YYYY-MM-DD"), dates.end.format("YYYY-MM-DD"));
        });
    }
    
    private fetchCustomers() {
        this.api.get('qa/v1/secm/customers').subscribe(result => {
            let customers = result.json()["customers"] as CustomerDTO[];

            // Set observable
            this.customersSubject.next(customers);
            return;
        });
    }

    private fetchJobsites(from: string, to: string) {
        this.api.get('qa/v2/dm/jobsites/summary?dateFrom=' + from + ' 00:00&dateTo=' + to + ' 23:59').subscribe(result => {
            let jobsites = result.json()["jobsites"] as JobsiteDTO[];

            // Set observable
            this.jobsitesSubject.next(jobsites);
            this.loadingSubject.next(false);
            return false;
        });
    }
}