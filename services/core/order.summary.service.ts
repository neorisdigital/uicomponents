import { Injectable }      from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Listeners
import { CalendarEvents } from '../../events/calendar.event'
import { SidebarEvents } from '../../events/sidebar.event'

// Api
import { ApiService } from '../../services/http.api.service';

import JobsiteDTO from '../../DTO/jobsiteDTO'

@Injectable()
export class OrderSummaryService {
    private ordersSummarySubject = new BehaviorSubject<any>(null);
    private orders$ = this.ordersSummarySubject.asObservable();

    private lock: boolean = false;

    private dates: any = null;
    private jobsite: JobsiteDTO = null;

    constructor(private api: ApiService, private calendar: CalendarEvents, private sidebar: SidebarEvents) {
        this.calendar.getDates().subscribe(dates => {
            if (!dates) { return; }
            this.dates = dates;
            if (!this.lock) { this.fetchOrders(); }
        });

        this.sidebar.getJobsiteSelected().subscribe(jobsite => {
            if (!jobsite) { return; }
            this.jobsite = jobsite;
            if (!this.lock) { console.log("Jobsite clicked"); this.fetchOrders(); }
        });
    }

    private fetchOrders() {
        // Validations
        if (!this.jobsite) { return; }
        if (!this.dates) { return; }

        this.lock = true;
        let from = this.dates.start.format("YYYY-MM-DD");
        let to = this.dates.end.format("YYYY-MM-DD");
        this.api.get('qa/v2/sm/summary/jobsites/' + this.jobsite.jobsiteId + '/orders?startdate=' + from + ' 00:00&enddate=' + to + ' 23:59').subscribe(orders => {
            // Set observable
            this.ordersSummarySubject.next(orders.json());
            this.lock = false;
            return;
        });
    }

    // Quick (single) order get
    public fetchOrder(orderId: number) {
        return this.api.get('qa/v2/sm/summary/jobsites/orders/' + orderId);
    }

    getOrdersSummary() {
        return this.orders$;
    }
}