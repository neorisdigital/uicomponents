import {CredentialsConstants} from '../constants/credentials.constants';
export class Logger{
    static log(value:any):void{
        if(CredentialsConstants.workingEnv=="development") {
            console.log(value);
        }
    }
    static error(value:any):void{
        if(CredentialsConstants.workingEnv=="development") {
            console.error(value);
        }
    }
    static logf(value:any):void{
        console.log(value);
    }
}