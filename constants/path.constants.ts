import {CredentialsConstants} from './credentials.constants';
export class PathConstants {


    static workingPath="";
    static workingEnv="development";
    
    //base paths to start the call
    static basePath="https://api.us.apiconnect.ibmcloud.com/";
    static basePathDev="http://other/path/";
    
    //part of the middle of the URL
    static prodURL="cnx-gbl-org-production/production/v1/";
    static qaURL="cnx-gbl-org-quality";
    static devURL="cnx-gbl-org-quality";
    static versionUrlV1 ="/qa/v1/";
    static versionUrlV2 ="/qa/v2/";
    static versionUrlV3 ="/qa/v3/";
    static versionUrl   ="/qa/api/";

    //real paths
    static LOGIN_PATH =  PathConstants.versionUrlV1 + "secm/oam/oauth2/token";
    static USER_CONFIG = PathConstants.versionUrlV1 + "secm/customers";
    static USERS_PATH = PathConstants.versionUrlV1 + "secm/users";
    static USER_PATH = PathConstants.versionUrlV1 + "secm/users/";
    static RECOVER_PATH = PathConstants.versionUrlV1 + "secm/recoverpwd";
    static USER_INFO= PathConstants.versionUrlV1 + "secm/myinfo";
    static REQUEST_HISTORY_PATH =PathConstants.versionUrlV1 +  "dm/orderrequest/plants?";
    static REQUEST_PATH =PathConstants.versionUrlV1 +  "dm/orderrequest";
    static PlANTS_PATH = PathConstants.versionUrlV1 + "dm/plants";
    static BRIEF_PLANTS_PATH =PathConstants.versionUrlV1 +  "dm/orderrequest/group"
    static PLANT_PATH = PathConstants.versionUrlV1 + "dm/plants/plant";
    static UPDATE_REQUEST_PATH = PathConstants.versionUrlV1 + "dm/orderrequest/request";
    static JOBSITES_FAVORITE_PATH = PathConstants.versionUrl + "secm/jobsites/favorites";
    static JOBSITES_PATH = PathConstants.versionUrlV2 + "sm/summary/jobsites/";
    static TICKET_PATH = PathConstants.versionUrlV3 +"dm/tickets?include=ticketitems,statuslog,shipment,driver&orderId=";
    static MARKER_PATH = PathConstants.versionUrl +"dtm/trucklocation/order/";

    
    static getWorkingPath():string{
       if(this.basePathDev=="http://other/path/"){
           //development environment
           this.workingPath=this.basePath;
       } else {
           this.workingPath=this.basePathDev;
       }
       switch(this.workingEnv){
           case "development":
           this.workingPath=this.workingPath+this.devURL;
           break;
           case "quality":
           this.workingPath=this.workingPath+this.qaURL;
           break;
           case "production":
           this.workingPath=this.workingPath+this.prodURL;
           break;
       }
        return this.workingPath;
    }

    /**
     * Possible values for environment
     * development
     * quality
     * production
     */
   static setEnv(env:string):void{
       if(env=="development"){
           this.workingEnv="development";
       } else if(env=="quality"){
           this.workingEnv="quality";
       } else if(env=="production"){
           this.workingEnv="production";
       }
       CredentialsConstants.setEnv(this.workingEnv);
   }
}