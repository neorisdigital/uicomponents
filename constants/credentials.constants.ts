export class CredentialsConstants {
    static workingEnv="development";
    static workingKey="";
    static workingAppCode="Foreman_App";
    static workingJwt="";
    static workingBearer="";

    static keyDev = "f624df7f-9b35-41c0-8a41-4854b86c84f1";
    static keyQa = "f624df7f-9b35-41c0-8a41-4854b86c84f1";
    static keyProd = "f624df7f-9b35-41c0-8a41-4854b86c84f1";
 
    static setAppCode(appCode: string): void {
        this.workingAppCode = appCode;
    }

    static getBearer(): string {
        return this.workingBearer;
    }

    static setBearer(value: string) {
        this.workingBearer = value;
    }

    static setKeys(dev: string, qa: string, prod: string) {
        this.keyDev = dev;
        this.keyQa = qa;
        this.keyProd = prod;
    }

    static getAppCode(): string {
        return this.workingAppCode;
    }

    static setJWT(jwt: string): void {
        this.workingJwt = jwt;
    }

    static getJWT(): string {
        return this.workingJwt;
    }

    static getKey(): string {
        if (this.workingEnv === "development") {
            return this.keyDev;
        } else if (this.workingEnv === "quality") {
            return this.keyQa;
        } else if (this.workingEnv === "production") {
            return this.keyProd;
        }
    }

    static setEnv(env: string): void {
        if (env === "development") {
            this.workingEnv = "development";
        } else if (env === "quality") {
            this.workingEnv = "quality";
        } else if (env === "production") {
            this.workingEnv = "production";
        }
    }
}