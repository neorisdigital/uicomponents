export default class OAuthDTO{
    access_token:string;
    expires_in:number;
    scope:string;
    refresh_token:string;
}