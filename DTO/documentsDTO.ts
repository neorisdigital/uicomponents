export default class DocumentsDTO {

    referenceNumber: string;
    folioDocument: number;
    jobSiteCode: number;
    jobSiteDesc: string;
    contract: number;
    totalContracts: number;
    purchaseOrder: number;
    documentTypeCode: string;
    createDate: string;
    dueDate: string;
    amount: number;
    balance: number;
    discount: string;

}