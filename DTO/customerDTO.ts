import JobsiteDTO from './jobsiteDTO'

export default class CustomerDTO {
    customerId: number;
    customerName: string;
    customerCode: string;
    regions: any[];
    applications: any[];
    jobsites: JobsiteDTO[];
}