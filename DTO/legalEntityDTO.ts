interface ResponseLegalEntity{
    company?: Array<Object>;
    customer?: Array<Object>;
    jobSite?: Array<Object>;
}

export interface LegalEntity{
    ResponseLegalEntity: ResponseLegalEntity;
}