//sample data
/*

dateCreate: null
label: "DF12 MX-PD-011 MORONES PRIETO"
monitoredBy: [{user: "Guillermo Salinas", userAccount: "e-gsalinass"}, {user: "Fernando Torres", userAccount: "f-ftorres"}, {user: "Julio Chavez", userAccount: "f-jchavez"}, {user: "User Driver 11", userAccount: "user.driver11@hotmail.com"}, {user: "User Driver 1", userAccount: "user.driver01@hotmail.com"}, {user: "User Driver neoris2", userAccount: "user.dispatch02@neoris.com"}, {user: "User Driver neoris4", userAccount: "user.dispatch04@neoris.com"}, {user: "User Dispatch 07", userAccount: "user.dispatch07@neoris.com"}, {user: "User Dispatch 08", userAccount: "user.dispatch08@neoris.com"}, {user: "User Dispatch 09", userAccount: "user.dispatch09@neoris.com"}, …]
plantCode: "DF12"
plantDesc: "MX-PD-011 MORONES PRIETO"
plantId: "213"
plantMonitoredByThisUser: false
score: 12
totalRequest: "0"
value: "DF12"

*/
import {UserDTO} from './userDTO'


export class PlantDTO {
  plantId: string;
  label: string;
  dateCreate: string;
  monitoredBy: UserDTO[];

}